# Doliprane

Python module for dealing with environment and path formatting, destined to be an asset manager

template module : 

```
from template import Template

path = 'Z:/shots/sh005/lighting/ACS_sh005_lighting_002.blend'

tpl_shot = Template('{root}/shots/sh{shot:04d}/{task}/{project}_sh{shot:04d}_{task}_{version:03d}.{extension}')

tpl_shot.parse(parse)
-- {'root' : 'Z:/', 'shot' : 5, 'task': 'lighting', 'version' : 2, 'extension' : 'blend'}

data = {'root' : 'Z:/', 'shot' : 8, 'task': 'layout', 'version' : 6, 'extension' : 'blend'}
tpl_shot.parse(data)
-- 'Z:/shots/sh008/layout/ACS_sh008_layout_006.blend'
```


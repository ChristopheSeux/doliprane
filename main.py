import sys
from PySide2.QtWidgets import QApplication, QMainWindow, QStyleFactory
from PySide2.QtCore import Qt

import os
import gazu


if __name__ == '__main__':
    print(sys.argv)
    if len(sys.argv)>1 and sys.argv[1].endswith('.py') :
        sys.argv = sys.argv[1:]
        exec(open(sys.argv[0]).read())

    else :
        os.environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
        QApplication.setAttribute(Qt.AA_EnableHighDpiScaling, True) #enable highdpi scaling
        QApplication.setAttribute(Qt.AA_UseHighDpiPixmaps, True)
        app = QApplication(sys.argv)
        QApplication.setStyle('Windows')

        from doliprane.main_window import MainWindow

        window = MainWindow()
        window.show()

        app.exec_()

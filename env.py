import os
from os import environ
from os.path import *
import platform
import yaml
from pathlib import Path


class Env :
    def __init__(self, *envs):
        self.platform = platform.system()
        self.envs = [Path(e) for e in envs]

        self.data = self._merge_dict([self._read_env(e) for e in self.envs])

        self.set()

    def _merge_dict(self, dicts) :
        data = {}
        for d in dicts :
            data.update(d)

        return data

    def _read_yml(self, path) :
        return yaml.safe_load( path.read_text() )

    def _read_env(self, path):
        if not path.exists() :
            raise Exception(f'The env "{path}" does not exist')

        data = self._read_yml(path)

        for p in ['Darwin','Linux','Windows'] :
            if not p in data : continue

            if p == self.platform :
                data.update(data.pop(p))
            else :
                del data[p]

        data['ENVS'] = os.pathsep.join([str(e) for e in self.envs])

        return data

    def set(self) :
        for k, v in self.data.items() :
            environ[k] = expandvars(str(v))

    def __setitem__(self, key, value):
        self.data[key] = value

        self.set()

    def __getitem__(self, key):
        return self.data[key]

    def __repr__(self) :
        text = '\nEnv(\n'
        for k, v in self.data.items() :
            text+= f"    {k} : {v}\n"
        text+= ')\n'

        return text

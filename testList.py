import sys
from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *

import os

class ListWidgetItem(QListWidgetItem) :
    def __init__(self, parent=None):
        super().__init__(parent)


class ListWidget(QListWidget) :
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setFlow(QListView.LeftToRight)
        self.setSizeAdjustPolicy(QListWidget.AdjustToContents)

        self.setMinimumWidth(1)
        #self.viewport().layout().setSizeConstraint(QLayout.SetMinimumSize)
        self.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Expanding)


class CustomQWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        label = QLabel("I am a custom widget")
        label.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.setLayout(QHBoxLayout())

        self.layout().addWidget(label)


if __name__ == '__main__':

    app = QApplication(sys.argv)
    window = QWidget()


    list = ListWidget()

    list.addItem('Test')
    list.addItem('BlaBla')

    print(list.sizeHintForColumn(0))
    '''
    item = ListWidgetItem(list)
    item_widget = CustomQWidget()
    item.setSizeHint(item_widget.sizeHint())
    list.addItem(item)
    list.setItemWidget(item, item_widget)


    item2 = ListWidgetItem(list)
    item_widget2 = CustomQWidget()
    item2.setSizeHint(item_widget2.sizeHint())
    list.addItem(item2)
    list.setItemWidget(item2, item_widget2)
    '''


    window_layout = QHBoxLayout(window)
    window_layout.addWidget(list)
    window_layout.addStretch()
    window.setLayout(window_layout)

    window.show()

    sys.exit(app.exec_())

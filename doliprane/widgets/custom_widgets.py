
from PySide2.QtWidgets import QSizePolicy
from PySide2.QtCore import Qt, QEvent
from PySide2.QtGui import QPixmap, QImage

from doliprane.constants import *
from doliprane.widgets.basic_widgets import *
from doliprane.widgets.properties import *

import re
import json
import sys
import subprocess
from pathlib import Path
import inspect
#from docopt import docopt


class UIList(HWidget):
    def __init__(self, parent=None, name='') :
        super().__init__(parent)

        if name :
            self.label = TextLabel(self, name)

        self.list_widget = VListWidget(self, tag='UIList')

        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)

        self.btn_widget = VWidget(self, align='Top')

        self.add_btn = PushButton(self.btn_widget, connect=lambda : self.add(),
        icon='plus_32', tag='Transparent', IconSize='small')
        self.remove_btn = PushButton(self.btn_widget, connect=lambda : self.remove(),
        icon='minus_32', tag='Transparent', IconSize='small')

        self.btn_widget.layout.addStretch()

    def add(self, text='') :
        print('Adding', text, 'to the projects list')
        self.list_widget.addItem(text)
        self.list_widget.setCurrentItem(self.list_widget.items()[-1])
        #return String(parent=self.list_widget, value=text)
    def remove(self, text='') :
        print('Remove', text, 'to the projects list')
        self.list_widget.takeItem(self.list_widget.currentRow())
        #self.list_widget.addItem(text)
        #return String(parent=self.list_widget, value=text)


    def to_list(self) :
        return [i.text() for i in self.list_widget.items()]


class Thumbnail(ImageLabel) :
    tooltip = Signal()
    def __init__(self, parent, image=None, size=(48,32) ) :
        super().__init__(parent, tag='Thumbnail')

        self.setFixedSize(*size)
        self.set_pixmap(image)

    def set_pixmap(self, image) :
        pix = None
        if isinstance(image, (str, Path) ) :
            image_path = Path(image)
            if image_path.exists() :
                pix = QPixmap(image_path.as_posix())
        elif isinstance(image, QImage) :
            pix = QPixmap.fromImage(image)

        if pix :
            pix = pix.scaled(self.size(), Qt.KeepAspectRatioByExpanding, Qt.SmoothTransformation)
            self.setPixmap(pix)
        else :
            self.setPixmap(QPixmap())

    def event(self, e) :
        super().event(e)

        if e.type() == QEvent.ToolTip :
            print('We have a tooltip begin')

        return True

class ActionButton(PushButton) :
    def __init__(self, action, parent=None, icon=None, draw_text=False,
    cursor=None, tooltip='', tag=None, **kargs) :

        super().__init__(parent=parent, text=action.name if draw_text else '',
        icon=icon, cursor=cursor, tooltip=tooltip, **kargs)

        self.action = action
        self.arguments = action.arguments
        self.path = action.path
        self.name = action.name
        self.icon = None

        self.dialog = VDialog(parent=parent, title=self.name, Spacing='normal', Padding='big',
        icon=self.icon)

        form_widget = FWidget(self.dialog, Spacing='normal')

        self.widgets = []
        for arg in self.arguments :
            if arg.data_path : continue
            text = arg.name
            if arg.type is bool :
                field = Bool(value=arg.default, name=arg.name)
                text = ''
            elif arg.type is str :
                print('>>> ARG', arg.name)
                field = String(value=arg.default)
            elif arg.type is int :
                field = Int(value=arg.default, decimals=0)
            elif arg.type is float :
                field = Float(value=arg.default)
            elif arg.type in (tuple, list) :
                field = Enum(value=arg.default, items=arg.choices)

            form_widget.layout.row(text,  field)
            self.widgets.append( field )

        ok_btn = PushButton(parent=self.dialog, text='OK', connect=self.valid)
        ok_btn.setAutoDefault(False)

        self.clicked.connect(self.dialog.exec_)

    def valid(self) :
        args = {k.norm_name : w.value for k,w in zip(self.arguments, self.widgets)}
        self.action.run(**args)

        self.dialog.close()

class PythonAction(WidgetAction) :
    def __init__(self, menu, action) :

        self.action = action
        self.arguments = action.arguments
        self.path = action.path
        self.name = action.name
        self.icon = None

        #### Test to read a data dict a the top of the python file
        '''
        self.script = Path(script_path).read_text()

        infos = re.findall(r' *?info *?= *?({[\w\n: _\.,\'\"{}\[\]]+})', self.script)

        self.raw_info = infos[0] if len(infos) else None
        self.info = eval(self.raw_info) if self.raw_info else {}

        name = self.info.get('_name', script_path.stem.replace('_', ' ').title() )
        icon = self.info.get('_icon')
        '''


        self.dialog = VDialog(title=self.name, Spacing='normal', Padding='big',
        icon=self.icon)

        form_widget = FWidget(self.dialog, Spacing='normal')

        self.widgets = []
        for arg in self.arguments :
            text = arg.name
            field = None
            if arg.type is bool :
                field = Bool(value=arg.default, name=arg.name)
                text = ''
            elif arg.type is str :
                field = String(value=arg.default)
            elif arg.type is int :
                field = Int(value=arg.default, decimals=0)
            elif arg.type is float :
                field = Float(value=arg.default)
            elif arg.type in (tuple, list) :
                field = Enum(value=arg.default, items=arg.choices)

            form_widget.layout.row(text,  field)

            self.widgets.append( field )

        ok_btn = PushButton(parent=self.dialog, text='OK', connect=self.valid)
        ok_btn.setAutoDefault(False)

        super().__init__(menu, icon=self.icon, text=self.name, connect=self.dialog.exec_)

    def _get_func_from_path(self, path, func_name) :
        from importlib import util

        spec = util.spec_from_file_location(func_name, path)
        mod = util.module_from_spec(spec)

        spec.loader.exec_module(mod)

        return getattr(mod, func_name)


    def valid(self) :
        '''
        if self.info :
            info = self.info.copy()
            for k, v in self.widgets.items() :
                value = v.value
                if isinstance(self.info[k], (tuple, list)) :
                    value = v.items.copy()
                    index = value.index(v.value)
                    value[0], value[index] = value[index], value[0]

                info[k] = value
        '''

        cmd = [sys.executable, str(self.path)]

        for argument, widget in zip( self.arguments, self.widgets) :
            if argument.type is bool :
                if widget.value :
                    cmd += [argument.arg]
            else :
                cmd += [argument.arg, widget.value]

        print(cmd)

        subprocess.Popen(cmd)

        #info_replace = str(info)+'\n'*self.raw_info.count('\n')
        #exec_script = self.script.replace(self.raw_info, info_replace, 1)

        #### Test with getting the help message
        #cmd = [sys.executable, str(self.path), '--help']

        #p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        #stdout, stderr = p.communicate()
        #help = stdout.decode("utf-8")


        self.dialog.close()

class AssetWidget(HWidget) :
    def __init__(self, asset_data):
        super().__init__(tag='AssetList', props={'Hpadding': 'normal'})
        self.label = TextLabel(self, asset_data['name'])

class AssetListWidget(ListWidget) :
    value_changed = Signal(str)

    def __init__(self, parent=None, items=[], tag=None, props={}):
        self.props = {'Hmargin': 'normal', 'Vmargin': 'normal'}
        super().__init__(parent, tag='AssetList', props=self.props)

        self.setAlternatingRowColors(True)
        self.setSizeAdjustPolicy(QListWidget.AdjustToContents)

        for i in items :
            self.addItem(i)

        self.currentItemChanged.connect(self.on_item_changed)


    def widgets(self) :
        items = [self.items(i) for i in self.count()]
        widgets = [self.itemWidget(i) for i in items]
        return [w for w in widgets if w]

    def addWidget(self, widget, name) :
        item = QListWidgetItem(self)
        item.name = name
        item_widget = widget
        item.setSizeHint(widget.sizeHint())
        self.setItemWidget(item, item_widget)
        super().addItem(item)

        if not self.currentItem() :
            self.setCurrentItem(item)

    def on_item_changed(self, i, j) :
        item = i if i else j
        self.value_changed.emit(item.name)

import argparse




def arg_parse(path='', string='', int=0.1, float=1, list='pomme', bool=True) :
    print('path', path)
    print('string', string)
    print('int', int)
    print('float', float)
    print('list', list)
    print('bool', bool, type(bool))



if __name__ == '__main__' :
    parser = argparse.ArgumentParser(description='Process some integers.',
    formatter_class= argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--path', default='./', type=str, help='My path')
    parser.add_argument('--string', default='string', type=str, help='My String')
    parser.add_argument('--int', default=1, type=int, help='My int')
    parser.add_argument('--float', default=0.1, type=float, help='My float')
    parser.add_argument('--list', default='pomme', choices=('pomme', 'poire'), type=str, help='My list')
    parser.add_argument('--bool', action='store_true')

    args = parser.parse_args()

    #print(vars(args))
    #synchronise_with_kitsu(**vars(args))

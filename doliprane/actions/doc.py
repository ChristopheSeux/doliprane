"""
Usage:
    arguments_example.py [options]

Process FILE and optionally apply correction to either left-hand side or
right-hand side.

Arguments:
  FILE        optional input file
  CORRECTION  correction angle, needs FILE, --left or --right to be present

Options:
  -h --help             help
  --create-dirs         verbose mode [default: True].

"""
from docopt import docopt


if __name__ == '__main__':
    arguments = docopt(__doc__)
    print(arguments)

print('Arguments')
print(arguments)

import argparse
import json


def sync_shots_with_kitsu(project, kitsu_shots, create_dir=True, add_to_kitsu=True) :
    print('project', project, type(project))
    print('kitsu_shots_id', kitsu_shots_id)
    print('create_dir', create_dir)
    print('add_to_kitsu', add_to_kitsu)



if __name__ == '__main__' :
    parser = argparse.ArgumentParser(description='Process some integers.',
    formatter_class= argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--project', required=True, type=json.loads)
    parser.add_argument('--kitsu-shots-id', required=True)
    parser.add_argument('--create-dir', default=True, choices = (True, False), type=eval)
    parser.add_argument('--add-to-kitsu', default=True, choices = (True, False), type=eval)

    args = parser.parse_args()

    sync_shots_with_kitsu(**vars(args))

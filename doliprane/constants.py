from pathlib import Path
from PySide2.QtWidgets import QApplication
import yaml

MODULE_DIR = Path(__file__).parent

ICON_DIR = MODULE_DIR/'resources'/'icons'
STYLESHEET = (MODULE_DIR/'stylesheet.css').read_text()
#CONFIG = yaml.safe_load( (MODULE_DIR/'conf.yml').read_text() )
#if not isinstance( CONFIG['projects'], (list, tuple) ) :
#    CONFIG['projects'] = [ CONFIG['projects'] ]
#if not isinstance( CONFIG['environments'], (list, tuple) ) :
#    CONFIG['environments'] = [ CONFIG['environments'] ]


STYLE = {'ICON_DIR': ICON_DIR.as_posix()}

#envs = os.scandir( abspath('env') )
ENV_DIR = MODULE_DIR/'envs'
TEMPLATE_DIR = MODULE_DIR/'templates'
CMD_DIR = MODULE_DIR/'cmds'
ACTIONS_DIR = MODULE_DIR/'actions'

APP = QApplication.instance()



#ENVS = [read_yml(f.path) for f in envs if f.name[-4:]=='.yml' and f.name[:1]!='_']

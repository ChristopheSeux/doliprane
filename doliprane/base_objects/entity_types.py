

class EntityType :
    def __init__(self, project, data) :
        self.project = project
        self.data = data
        self.id = data['id']
        self.name = data['name']
        self.norm_name = project.templates.norm_str(self.name)


from PySide2.QtWidgets import QApplication
from PySide2.QtCore import Signal, QSettings

from pathlib import Path
from os import chdir
import yaml
import json
from copy import deepcopy
import os
from time import time
import sys

from template import Templates
from env import Env

from doliprane.constants import *
from doliprane.base_objects.conf import Conf
from doliprane.base_objects.soft import Soft
from doliprane.base_objects.collection import Collection
from doliprane.base_objects.task_types import AssetTaskTypes, ShotTaskTypes
from doliprane.base_objects.entities import Assets, Shots, Sequences
from doliprane.base_objects.entity_types import EntityType
from doliprane.base_objects.task_status import TaskStatus
from doliprane.base_objects.person import Person
from doliprane.base_objects.action import Action


class Project(Conf) :
    def __init__(self, conf) :
        super().__init__(conf)

        print('CONF', conf)

        self.signals = APP.signals
        kitsu = APP.kitsu

        #print('self.conf', self.conf)
        if not self.conf or not isinstance(self.conf, dict):
            return

        self.name = self.conf['name']

        self.env = Env(self.conf['env'] )

        #print(self.env)

        self.name = self.conf['name']
        self.settings = QSettings(self.name, 'doliprane')

        self.templates = Templates(self.conf['templates'] )
        #print(self.conf['templates'])

        # Store templates path to the env, is it really the good place to do that?
        os.environ['TEMPLATES'] = os.pathsep.join( [i.as_posix() for i in self.conf['templates'] ] )

        local_root = self.settings.value('local_root', self.env.data['LOCAL_ROOT'] )
        self.set_local_template(local_root)

        self.softs_dir = Path(local_root)/'softs'
        softs = [Soft(self, {'hide':True})] # Default soft

        # Add Python as soft
        softs += [Soft(self, {'name':'Python', 'hide':True, 'path': sys.executable})]

        softs +=  [ Soft(self, s) for s in self.conf.get('softs', []) ]
        self.softs = Collection.from_attr('name', softs)

        actions = [Action(self, a) for a in self.conf.get('actions', []) ]
        self.actions = Collection.from_attr('name', actions)

        self.persons = []
        self.task_status = []
        self.assets = Assets(self, self.conf['assets'])
        self.shots = Shots(self, self.conf['shots'])
        self.sequences = Sequences(self, self.conf['sequences'])

        kitsu.project_updated.connect(self.set_project)
        kitsu.user_updated.connect(self.set_user)
        kitsu.persons_updated.connect(self.set_persons)
        kitsu.task_types_updated.connect(self.set_task_types)
        kitsu.assets_updated.connect(self.assets.set)
        kitsu.shots_updated.connect(self.shots.set)
        kitsu.sequences_updated.connect(self.sequences.set)
        kitsu.entity_types_updated.connect(self.set_entity_types)
        kitsu.task_status_updated.connect(self.set_task_status)
        #kitsu.my_tasks_updated.connect(self.set_my_tasks)

        self.shot_type = None
        self.sequence_type = None
        self.episode_type = None
        self.scene_type = None
        self.asset_types = Collection()

        self.asset_task_types = Collection()
        self.shot_task_types = Collection()

        #print(self.templates)

        APP.kitsu.connected.connect(self.set_casting)

    def set_casting(self) :
        for a in self.assets :
            a.set_casting()
        for s in self.shots :
            s.set_casting()

    def set_local_template(self, local_root) :
        local_templates = deepcopy(self.templates.raw)
        types = deepcopy(self.templates.types)

        local_templates['root'] = local_root

        self.local_templates = Templates(local_templates, types=types )

    @property
    def entity_types(self) :
        return Collection(dict(**self.asset_types.to_dict(),
        shot=self.shot_type, sequence=self.sequence_type) )

    @property
    def task_types(self) :
        return Collection({**self.asset_task_types.to_dict(), **self.shot_task_types.to_dict()} )

    @property
    def tasks(self) :
        asset_tasks = [t for a in self.assets for t in a.tasks]
        shot_tasks = [t for a in self.shots for t in a.tasks]

        return asset_tasks+shot_tasks

    def my_tasks(self) :
        tasks = [t for t in self.tasks if APP.user in t.assignees]
        tasks.sort(key=lambda x : x.entity.label)

        return tasks

    def set_project(self, data) :
        self.id = data['id']

        os.environ['KITSU_PROJECT_ID'] = self.id

    def set_user(self, data) :
        APP.user = Person(self, data)

    def set_persons(self, persons) :
        #print('Set Task Status')
        persons = [ Person(self, p) for p in persons if p['id']!= APP.user.id]
        self.persons = Collection.from_attr('full_name', [*persons, APP.user])
        self.signals.persons_updated.emit()

    def set_task_types(self, task_types) :
        asset_task_types = [t for t in task_types if not t['for_shots'] ]
        asset_task_types.sort(key=lambda x :x['priority'])
        self.asset_task_types = AssetTaskTypes(self, asset_task_types, self.conf['assets'])

        shot_task_types = [t for t in task_types if t['for_shots'] ]
        shot_task_types.sort(key=lambda x :x['priority'])
        self.shot_task_types = ShotTaskTypes(self, shot_task_types, self.conf['shots'])

        self.signals.task_types_updated.emit()

    def set_entity_types(self, entity_types) :
        self.asset_types = Collection()
        for e in entity_types :
            name = e['name']
            if name in ('Shot', 'Sequence', 'Episode', 'Scene') :
                setattr(self, f'{name.lower()}_type', EntityType(self, e) )
            else :
                asset_type = EntityType(self, e)
                self.asset_types[asset_type.norm_name] =  asset_type
        self.signals.entity_types_updated.emit()

    def set_task_status(self, task_status) :
        #print('Set Task Status')
        task_status = [ TaskStatus(self, t) for t in task_status ]
        self.task_status = Collection.from_attr('norm_name', task_status)
        self.signals.task_status_updated.emit()

        '''
    def set_assets(self, assets) :
        #print('Set Assets')
        assets = [ Asset(self, a) for a in assets ]
        self.assets = Collection.from_attr('name', assets)
        self.signals.assets_updated.emit()

    def set_shots(self, shots) :
        print('Set Shots')
        shots = [ Shot(self, s) for s in shots ]
        self.shots = Collection.from_attr('name', shots)
        self.signals.shots_updated.emit()


    def set_sequences(self, sequences) :
        #print('Set Sequences')
        sequences = [ Sequence(self, s) for s in sequences ]
        self.sequences = Collection.from_attr('name', sequences)
        self.signals.sequences_updated.emit()
        '''

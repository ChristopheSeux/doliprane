
from PySide2.QtWidgets import QApplication
from PySide2.QtGui import QColor

import yaml
from os.path import exists
from functools import partial
import re

class StyleSheet :
    def __init__(self, stylesheet, constants={}, prop_groups=()) :
        if exists(stylesheet) :
            with open(stylesheet, 'r') as data:
                self.stylesheet = data.read()
        else :
            self.stylesheet = stylesheet

        self.props = dict(constants, lighten=self.lighten, darken=self.darken)
        self.style_props = {}

        for p in prop_groups :
            for k, w in p.items() :
                self.style_props[k] = w
                w.value_changed.connect(self.update)

        self.update()


    def lighten(self, color, ratio=0.5):
        c = color
        if not isinstance(color, QColor) :
            c = QColor(*color)

        c = c.lighter(100+ ratio*100)

        if isinstance(color, QColor) :
            return c
        else :
            return (c.red(), c.green(), c.blue(), c.alpha() )

    def darken(self, color, ratio=0.5):
        c = color
        if not isinstance(color, QColor) :
            c = QColor(*color)

        c = c.darker(100+ ratio*200)

        if isinstance(color, QColor) :
            return c
        else :
            return (c.red(), c.green(), c.blue(), c.alpha() )

    def update(self) :
        app = QApplication.instance()
        css = self.stylesheet

        attrs = dict(self.props, **{k:v.value for k,v in self.style_props.items()} )

        if 'ui_scale' in attrs :
            #print('DPX :', QApplication.instance().pixel_ratio)
            attrs['ui_scale'] *= QApplication.instance().pixel_ratio

        pattern = re.compile('\[(.*?)\]')
        matchs = [(e.groups()[0], e.group()) for e in pattern.finditer(css)]

        for result, match in matchs :
            if '=' in result : continue
            v = eval(result, attrs)
            css = css.replace(match, str(v),1)

        app.setStyleSheet(css)


from PySide2.QtWidgets import QApplication

from pathlib import Path
import shutil
import gazu
import json

from doliprane.base_objects.collection import Collection
from doliprane.base_objects.worker import Worker
#from doliprane.base_objects.dependency import Dependency
from doliprane.utils import copy, get_func_from_path, open_file
from doliprane.constants import *
from os.path import abspath
import os


class Dependency :
    def __init__(self, version, path):
        self.version = version
        self.relative_path = path
        self.local_path = Path( abspath( str(version.local_path.parent) + path ) )
        self.path = Path( abspath( str(version.path.parent) + path ) )
        self.type = 'FILE'

    def __repr__(self):
        return f'Dependency({self.relative_path})'


class Versions(Collection) :
    def __init__(self, project) :
        super().__init__()
        self.project = project

    def set(self, task, versions) :
        self.clear()
        for v in versions :
            version = Version(task, v)
            self[version.name] = version
        #self.project.signals.assets_updated.emit()

    def add(self, task, version) :
        v = Version(task, version)
        self[v.name] = v

        #self.project.signals.assets_updated.emit()

        return v


class Version() :
    def __init__(self, task, version) :
        self.task = task
        self.task_name = task.name
        self.task_type = task.task_type
        self.project = self.task.project
        self.soft = self.task_type.soft
        self.entity = self.task.entity

        self.version = version
        #print(self.entity.name)
        #print('self.path', self.path)
        #print('self.local_pat', self.local_path)
        container = self.project.env['REVIEW_CONTAINER']#, 'QUICKTIME')
        self.output_ext = 'mov' if container== 'QUICKTIME' else 'mp4'

        tpl = self.project.templates

        self.name = tpl['version_name'].format({'version': version })
        self.extension = self.task.format_data['ext']

        self.screenshot = self.task.tpl_screenshot.format(self.format_data)

        self.infos = {}
        self.dependencies = []


        #if self.file_infos.exists() :

        #self.frame_start
        #print('screenshot', self.screenshot)

        #self._playblast = self.conf.get('playblast')

        #if self._playblast and self._get_path.parent.suffix == '.py' :
        #    self._playblast = get_func_from_path(self._playblast.parent, self._playblast.stem)

    def get_file_infos(self, force=False):
        if self.infos and not force: return

        infos = {}
        if self.file_infos.exists() :
            self.infos = json.loads( self.file_infos.read_text() )

            if 'libraries' in self.infos :
                self.dependencies = [Dependency(self, d) for d in self.infos['libraries'] ]

            del self.infos['libraries']

    @property
    def file_infos(self) :
        return Path( self.task.tpl_infos.format(self.format_data) )

    @property
    def output(self) :
        return Path( self.task.tpl_output.format(self.format_data, ext=self.output_ext) )

    @property
    def local_output(self):
        return Path( self.task.tpl_local_output.format(self.format_data, ext=self.output_ext) )

    @property
    def path(self):
        return Path( self.task.tpl_file.format(self.format_data) )

    @property
    def local_path(self):
        return Path( self.task.tpl_local_file.format(self.format_data) )

    @property
    def work_in_local(self) :
        return APP.preferences.general.work_in_local

    @property
    def format_data(self) :
        return {**self.task.format_data, 'version' : self.version}

    def download(self) :
        print(f'Download version from {self.path} to {self.local_path}')
        #self.local_path.parent.mkdir(exist_ok=True, parents=True)
        #shutil.copy2(self.path, self.local_path)

        self.entity.download(versions=[self])

    def upload(self) :
        print(f'Upload version from {self.local_path} to {self.path}')
        #self.path.parent.mkdir(exist_ok=True, parents=True) # Create dir on network
        self.entity.upload(versions=[self])

        # Dependency

        #shutil.copy2(self.local_path, self.path)

    def to_dict(self) :
        path = self.local_path if self.work_in_local else self.path
        output = self.local_output if self.work_in_local else self.output

        self.get_file_infos()

        data = {
            'infos' : self.infos,
            'path': str(path),
            'task': self.task.name,
            'output': str(output),
            'type': self.entity.type.name,
            'version' : self.version
        }

        print('\n>>>>>>>> Version to dict', data)
        if data['type'] == 'Shot' :
            data['sequence'] = self.entity.sequence.number
            data['shot'] = self.entity.number

        return data

    def playblast(self, file=None, update_libraries=False, burn_stamps=True, add_to_kitsu=False, play_on_finish=False) :
        print('>>> Playblast', add_to_kitsu)
        data = self.to_dict()

        cmds = APP.project.shots.actions['Playblast'].get_cmds(
        files=[file or data['path']], update_libraries=update_libraries, output=data['output'] )

        print('\n >>>CMDS', cmds)

        APP.worker.add_cmd( *cmds )

        # Make the stamps

        if burn_stamps and APP.project.shots.actions.get('Stamps') :
            cmds = APP.project.shots.actions['Stamps'].get_cmds( shots=[data] )
            APP.worker.add_cmd( *cmds )
            #files=[file or data['path']], update_libraries=update_libraries, output=data['output'] )

        ## TEMPORARY ADD to EDITING PATH

        cmds = APP.project.shots.actions['Convert for edit'].get_cmds( shots=[data] )
        APP.worker.add_cmd( *cmds )

        if add_to_kitsu :
            APP.worker.add_job( self.publish, preview=data['output'] )

        if play_on_finish :
            #cmds = APP.project.shots.action['Play'].get_cmds('shots': [ data['output'] ] )
            #self.plablast_worker.add_cmd( *cmds )
            env = os.environ.copy()
            del env['OCIO']
            APP.worker.add_job(open_file, data['output'], env=env)


    def open(self) :
        # Check if file exist and build it if not existing
        #print(f'Open Task {self.name} from path {self.path}')

        env = {'TEMPLATE':self.task_type.tpl_file.string}

        path = self.local_path if self.work_in_local else self.path

        print(f'Open Task {self.name} from path {path}')

        self.soft.launch(version=self.task_type.soft_version, file=path, env=env)

    def pre_publish(self) :
        pass

    def publish(self, status=None, comment='', preview=None) :
        if status :
            status = self.project.task_status[status]
        else :
            status = self.task.status

        c = gazu.task.add_comment(self.task.id, status.id, comment)

        if preview :
            preview = gazu.task.add_preview(self.task.id, c, preview)

        print(f'Publish Task {self.name}')
        pass

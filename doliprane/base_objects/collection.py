
class Collection():
    def __init__(self, data={}):
        self._data = []

        for k, v in data.items() :
            self[k] = v

    @classmethod
    def from_attr(cls, attr, data) :
        collection = cls()
        for d in data :
            collection[getattr(d, attr)] = d
        return collection

    def clear(self) :
        self._data = []

    def to_dict(self) :
        return {k : v for k,v in self._data}

    def items(self) :
        return self.to_dict().items()

    def keys(self) :
        return self.to_dict().keys()

    def __setitem__(self, key, value):
        self._data.append((key, value))

    def __getitem__(self, key):
        if isinstance(key, int) :
            return self._data[key][1]
        elif isinstance(key, str) :
            return self.to_dict()[key]

    def __delitem__(self, key):
        if isinstance(key, int) :
            self._data.pop(key)

        elif isinstance(key, str) :
            i = next(i for i,k in enumerate(self._data) if k[0] == key)
            self._data.pop(i)

    def __len__(self):
        return len(self._data)

    def __iter__(self) :
        return (i[1] for i in self._data)

    def __repr__(self) :
        return f'{list(self.__iter__())}'

    def get_by_id(self, id) :
        #print(id, [v.id for k,v in self.items()])
        return {v.id : v for k, v in self._data }.get(id)

    def get(self, name) :
        if name in self.keys() :
            return self[name]

    def find_all(self, **kargs) :
        return [i for i in self if all([getattr(i, k)==v for k, v in kargs.items() ]) ]

    def find(self, **kargs) :
        result = self.find_all(**kargs)
        if result :
            return result[0]

    def last(self) :
        if len(self) :
            return self[-1]

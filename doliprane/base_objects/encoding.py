import ffmpeg
import PySide2
import sys
from PySide2.QtWidgets import QApplication, QWidget, QPushButton, QVBoxLayout, \
QHBoxLayout, QLabel, QSizePolicy

from PySide2.QtGui import QPixmap, QImage
from PySide2.QtCore import Qt
import tempfile
from pathlib import Path
import re
import datetime
import locale
import subprocess


class VWidget(QWidget) :
    def __init__(self, parent=None, spacing=0) :
        super().__init__()
        self.setLayout( QVBoxLayout() )
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().setSpacing(spacing)

        if parent :
            parent.addWidget(self)


class HWidget(QWidget) :
    def __init__(self, parent=None, spacing=0) :
        super().__init__()
        self.setLayout( QHBoxLayout() )
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().setSpacing(spacing)

        if parent :
            parent.addWidget(self)


class StampRow(HWidget) :
    def __init__(self, stamps_widget, parent=None) :
        super().__init__(parent=parent)

        self.stamps_widget = stamps_widget
        self.stamps = self.stamps_widget.stamps

        r = self.stamps.scale_ratio

        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        self.layout().setContentsMargins(20*r, 0, 20*r, 0)
        #self.layout().setSpacing(80*r)

        self.left_wgt = HWidget(self.layout() )
        self.layout().addStretch()
        self.center_wgt = HWidget(self.layout() )
        self.layout().addStretch()
        self.right_wgt = HWidget(self.layout()  )


class StampCol(VWidget) :
    def __init__(self, stamps_widget, parent=None) :
        super().__init__(parent=parent)

        self.stamps_widget = stamps_widget
        self.stamps = self.stamps_widget.stamps

        r = self.stamps.scale_ratio

        self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.layout().setContentsMargins(0, 20*r, 20*r, 20*r )
        #self.layout().setSpacing(80*r)

        self.top_wgt = VWidget(self.layout() )
        self.layout().addStretch()
        self.center_wgt = VWidget(self.layout() )
        self.layout().addStretch()
        self.bottom_wgt = VWidget(self.layout()  )


class HSeparator(HWidget) :
    def __init__(self, parent=None, spacing=5, text='', color=(150,150,150,255)) :
        super().__init__(parent=parent)

        self.setFixedWidth(spacing)

        self.layout().setAlignment(Qt.AlignHCenter)

        self.text = QLabel(text)
        self.text.setStyleSheet(f"QLabel {{color: rgba{color}; font-size: 12pt; }}")
        self.layout().addWidget(self.text)


class VSeparator(HWidget) :
    def __init__(self, parent=None, size=0) :
        super().__init__(parent=parent)

        self.setFixedHeight(size)

        self.layout().setAlignment(Qt.AlignVCenter)

class StampWidget(HWidget) :
    def __init__(self, stamp, parent=None) :
        super().__init__(spacing=0)

        self.stamp = stamp
        #stamp.stamp_widget = self

        self.field = QLabel()
        self.label = QLabel()

        self.layout().addWidget(self.field)
        self.layout().addWidget(self.label)

        #self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        #print('\n>>>>>>>>>>> STAMP Widget', self.stamp)
        #print(self.stamp.color)

        #self.setText(self.stamp.text)
        self.field.setStyleSheet(f"QLabel {{color: rgba{self.stamp.field_color}; font-size: 12pt; }}")
        self.label.setStyleSheet(f"QLabel {{color: rgba{self.stamp.color}; font-size: 12pt; }}")
        #setStyleSheet("font-size: 14pt;")

    #[CCB]
    def eval(self, frame) :

        self.field.setText(self.stamp.name + ' : ')
        self.field.setVisible(self.stamp.show_field and self.stamp.name!='')

        value = str(self.stamp.value)

        for expr in re.findall(r'{(.*?)}', value) :
            field = '{%s}'%expr

            if 'date' in expr :
                value = value.replace(field, self.stamp.format_date())

            elif 'name' in expr :
                value = value.replace(field, self.stamp.movie.name)

            elif 'frame' in expr :
                value = value.replace(field, field.format(frame=frame+self.stamp.movie.frame_start))

            elif 'timecode' in expr :
                value = value.replace(field, self.stamp.frames_to_TC(frame))

        self.label.setText( str(value) )


class StampsWidget(VWidget) :
    def __init__(self, stamps) :
        super().__init__()

        self.stamps = stamps
        self.spacing = self.stamps.spacing
        self.separator = self.stamps.separator

        self.movie = stamps.movie

        #print('\n>>>>> STAMPS', stamps)

        #self.layout().setContentsMargins(5, 5, 5, 5)
        self.setStyleSheet(f"QWidget {{ background-color: rgba(0,0,0,0) }}")
        self.setAttribute(Qt.WA_DontShowOnScreen)

        self.top_wgt = StampRow( parent=self.layout(), stamps_widget=self )

        if self.stamps.movie.vborder :
            print('vborder')
            self.top_wgt.setFixedHeight(self.stamps.movie.vborder)

        self.center_wgt = HWidget( parent=self.layout() )

        self.left_wgt = StampCol( parent=self.center_wgt.layout(), stamps_widget=self )

        self.center_wgt.layout().addStretch()

        self.right_wgt = StampCol( parent=self.center_wgt.layout(), stamps_widget=self )
        self.bottom_wgt = StampRow( parent=self.layout(), stamps_widget=self )

        if self.stamps.movie.vborder :
            self.bottom_wgt.setFixedHeight(self.stamps.movie.vborder)


        self.layouts = {
            "top_right" : self.top_wgt.right_wgt.layout(),
            "top_center" : self.top_wgt.center_wgt.layout(),
            "top_left" : self.top_wgt.left_wgt.layout(),
            "bottom_right" : self.bottom_wgt.right_wgt.layout(),
            "bottom_center" : self.bottom_wgt.center_wgt.layout(),
            "bottom_left" : self.bottom_wgt.left_wgt.layout(),
            "left_top" : self.left_wgt.top_wgt.layout(),
            "left_center" : self.left_wgt.center_wgt.layout(),
            "left_bottom" : self.left_wgt.bottom_wgt.layout(),
            "right_top" : self.right_wgt.top_wgt.layout(),
            "right_center" : self.right_wgt.center_wgt.layout(),
            "right_bottom" : self.right_wgt.bottom_wgt.layout()
        }
        #for stamp_data in self.stamps :
        #    self.add_stamp(stamp_data)
        self.separators = []

    def eval_stamps(self, frame) :
        #remove parent
        for separator in self.separators :
            separator.setParent(None)

        for stamp in self.stamps :
            stamp_widget = stamp.stamp_widget

            stamp_widget.setParent(None)
            stamp_widget.eval(frame)

            self.layouts[stamp.position].addWidget(stamp_widget)

            sep = HSeparator(spacing=self.spacing, text=self.separator, color=stamp.field_color )
            self.layouts[stamp.position].addWidget(sep)

            self.separators.append(sep)

        #Removing trailing sep
        for pos, layout in self.layouts.items() :
            last_widget_item = layout.itemAt(layout.count()-1)
            if not last_widget_item : continue

            last_widget = last_widget_item.widget()
            if last_widget and isinstance(last_widget, (HSeparator, VSeparator)) :
                last_widget.setParent(None)


    def render(self) :
        w, h = self.movie.output_width, self.movie.output_height

        for frame in range(self.movie.nb_frames) :

            self.eval_stamps(frame)

            self.show()
            self.resize(w, h)

            image = QImage(self.size(),  QImage.Format_ARGB32_Premultiplied)
            image.fill(Qt.transparent)
            super().render(image)
            pixmap = QPixmap.fromImage(image)

            render_output = Path(tempfile.gettempdir(), self.movie.basename,
            f'{self.movie.basename}_stamps_{frame+1:04d}.png')

            render_output.parent.mkdir(parents=True, exist_ok=True)

            pixmap.save(str(render_output))

            print(f'Save image to {render_output}')

            self.close()


class Stamps :
    def __init__(self, movie, stamps=[], separator='', spacing=80)  :

        #print('\n>>>>> STAMPS', stamps)

        self._data = []
        self.movie = movie

        self.scale_ratio = self.movie.output_height / 1080
        self.separator = separator
        self.spacing = spacing * self.scale_ratio
        #self.text_margin = self.movie.output_height / 54

        self.stamps_widget = StampsWidget(self)


        for stamp in stamps :
            self.add(stamp)

    def add(self, stamp_data) :
        self._data.append( Stamp(stamp_data, stamps=self) )
        #self.stamps_widget.add_stamp(stamp)

    def render(self) :

        render_output = Path(tempfile.gettempdir(), self.movie.basename, self.movie.basename+'_stamps_%04d.png')
        self.stamps_widget.render()

        return ffmpeg.input(render_output, framerate=24)

    def __setitem__(self, value):
        self._data.append( value )

    def __getitem__(self, key):
        return self._data[key]

    def __delitem__(self, key):
        self._data.pop(key)

    def __len__(self):
        return len(self._data)

    def __iter__(self) :
        return (i for i in self._data)


class Stamp :
    def __init__(self, data, stamps) :

        #print('Stamp', data)
        self.stamps = stamps
        self.movie = stamps.movie

        self.position = data.get('position', 'top_left')
        self.value = data['value']
        self.name = data.get('name', '')
        self.field_color = data.get('field_color', (150,150,150,255))
        #self.field = 'frame'
        #self.text = data['text']
        self.font_size = 14
        self.show_field = True
        self.color = data.get('color', (200,200,200,255))

        self.stamp_widget = StampWidget(self)

        #self.stamps.stamps_widget.add_stamp(self.stamp_widget)

    def format_date(self) :
        locale.setlocale(locale.LC_TIME, 'en_US.UTF-8')
        return datetime.date.today().strftime("%B %d, %Y")

    def frames_to_TC (self, frame):
        h = int(frame / 86400)
        m = int(frame / 1440) % 60
        s = int((frame % 1440)/24)
        f = frame % 1440 % 24
        return ( "%02d:%02d:%02d:%02d" % ( h, m, s, f))


class Movie :
    def __init__(self, movie_data, stamps=[], output_width=None, output_height=None) :


        print('\n>>>>> MOVIE', movie_data)

        self.data = movie_data
        self.filepath = Path(self.data['filepath'])

        self.basename = self.filepath.stem
        self.name = movie_data.get('name', self.basename)
        self.frame_start = movie_data.get('frame_start', 0)

        self.probe = ffmpeg.probe(self.filepath)

        self.video_channel = next((s for s in self.probe['streams'] if s['codec_type']=='video'), None)
        self.audio_channel = next((s for s in self.probe['streams'] if s['codec_type']=='audio'), None)

        self.nb_frames = int(self.video_channel['nb_frames'])

        self.input_width = self.video_channel['coded_width']
        self.input_height = self.video_channel['coded_height']

        self.output_width = output_width or self.input_width
        self.output_height = output_height or self.input_height

        self.vborder = (self.output_height - self.input_height) / 2
        self.hborder = (self.output_width - self.input_width) / 2

        self.stamps = Stamps(self, stamps)

        #self.fontsize = self.output_height/50

    def add_stamp(self, stamp) :
        self.stamps.add( stamp )

    def set_stamps(self, stamps, separator) :
        self.stamps = Stamps(self, stamps, separator)

    @property
    def audio_stream(self) :
        return ffmpeg.input( str(self.filepath) ).audio

    @property
    def video_stream(self) :
        iw, ih = self.input_width, self.input_height
        ow, oh = self.output_width, self.output_height

        stream = (
            ffmpeg.input( str(self.filepath) )
            .filter('scale', size=f'{iw}:{ih}', force_original_aspect_ratio='decrease')
            .filter('pad', height=f'{oh}', y=f'({oh}-{ih})/2' )
        )

        return stream

    def render_video(self) :
        # apply stamp
        stamps_ovelay = self.stamps.render()

        render_output = Path(tempfile.gettempdir(), self.basename, self.basename+'_%04d.png')
        (
        self.video_stream
        .overlay(stamps_ovelay)
        .output( str(render_output))
        .run(overwrite_output=True)
        )

        return ffmpeg.input(render_output, framerate=24)

    def render_audio(self) :
        render_output = Path(tempfile.gettempdir(), self.basename, self.basename+'.wav')

        subprocess.call(['ffmpeg', '-y', '-i', str(self.filepath),'-af', 'aresample=async=1', str(render_output)] )

        #(
        #self.audio_stream
        #.output( str(render_output) )
        #.run(overwrite_output=True)
        #)

        return ffmpeg.input(render_output)



class Encoding :
    def __init__(self, movies=[], output_width=None, output_height=None) :

        if not QApplication().instance() :
            app = QApplication(sys.argv)
            QApplication.setStyle('Windows')

        self.output_width = output_width
        self.output_height = output_height

        self.movies = []

        for m in movies :
            self.add_movie(m)

    def set_stamps(self, stamps, separator=True) :
        for m in self.movies :
            m.set_stamps(stamps, separator)

    def add_movie(self, movie_data, stamps=[]) :
        ow, oh = self.output_width, self.output_height
        self.movies.append( Movie(movie_data, output_width=ow, output_height=oh) )


    def render(self, output) :
        ## Don't forgot the sound !!

        video_streams = [m.render_video() for m in self.movies]
        audio_streams = [m.render_audio() for m in self.movies]

        if len(video_streams) == 1 :
            video_streams= video_streams[0]
        else :
            video_streams = ffmpeg.concat(*video_streams)

        if len(audio_streams) ==1 :
            audio_streams = audio_streams[0]
        else :
            audio_streams = ffmpeg.concat(*audio_streams, v=0)


        (
        video_streams
        .concat(audio_streams, v=1, a=1)
        .output( output, crf=20, pix_fmt='yuv420p', g=4, framerate=24)
        .run(overwrite_output=True)
        )
        #for stream in self.streams :

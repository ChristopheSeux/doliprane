
import yaml
import json

from os import chdir
from os.path import expandvars
from pathlib import Path
from PySide2.QtCore import QObject


class Conf(QObject) :
    def __init__(self, conf) :
        super().__init__()

        if not isinstance(conf, dict) : #It's a path
            print('Reading ', conf)
            conf = Path(conf)

            if not conf.exists() :
                print(f'The Project conf file {conf} does not exist')
                self.conf = None
                return

            chdir(conf.parent)
            #chdir(str(conf.parent) )

            if conf.suffix == '.yml' :
                conf = yaml.safe_load(conf.read_text() )

            elif conf.suffix == '.json' :
                conf = json.loads(conf.read_text() )

        # Make all path absolute
        #self.cwd = conf.parent
        #print('CONFIGGGG', conf)


        self.conf = self.absolute_path(conf)


    def absolute_path(self, obj):
        """
        Recursivly goes through the dictionnary obj and replaces value with / or \\
        into absolute Path.
        """
        if isinstance(obj, dict):
            new = {}
            for k, v in obj.items():
                new[k] = self.absolute_path(v)
        elif isinstance(obj, list):
            new = []
            for v in obj:
                new.append(self.absolute_path(v))
        else:
            if isinstance(obj, str) and '$' in  obj :
                print('$$ in ', obj)
                obj = expandvars(obj)
                print('new_obj', obj)

            if isinstance(obj, str) and ('/' in obj or '\\' in obj) :
                if obj.startswith('./') :
                    obj = Path(obj).absolute()
                else :
                    obj = Path(obj)


            return obj

        return new

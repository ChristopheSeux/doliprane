
from PySide2.QtCore import QThread
import subprocess
from doliprane.constants import APP
import os

class Worker(QThread) :
    def __init__(self) :
        super().__init__()

        self.jobs = []
        APP.signals.closed.connect(self.close)
        self.is_running = True

        self.start()


    def add_job(self, instance, *args, **kargs) :
        self.jobs.append( (instance, args, kargs) )

    def add_cmd(self, cmd, **kargs) :
        kargs['env'] = os.environ.copy()

        print('\n\n>>>>>>>>>>>>>>>>><<', kargs['env']['PYTHONPATH'])
        #print('\n Add cmd', cmd, arg)
        self.jobs.append( (subprocess.call, [cmd], kargs) )

    def close(self) :
        self.is_running = False

    def run(self) :

        while self.is_running :



            if not self.jobs :
                self.sleep(0.2)
                continue

            job = self.jobs[0]
            print('\n>>>> Worker Thread Start', job)

            instance, args, kargs = job

            try :
                instance(*args, **kargs)
            except Exception as e:
                print(e)

            del self.jobs[0]


        print('\n>>>> Worker Thread Finished')

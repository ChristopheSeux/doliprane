
from PySide2.QtCore import QThread, Signal, QObject

from os import environ as env
from pathlib import Path
import gazu

from doliprane.constants import *


class LoginThread(QThread) :
    response = Signal(str)
    logged = Signal()

    def __init__( self, kitsu, url, login, password ) :
        super().__init__()

        self.kitsu = kitsu
        self.url = url
        self.login = login
        self.password = password

    def run(self) :
        print(f'Setting Host for kitsu {self.url}')
        gazu.client.set_host(self.url)
        try :
            gazu.client.host_is_up()
        except Exception as e :
            print('Host down \n', e)
            return

        res = None
        try :
            print(f'Log in to kitsu as {self.login}')
            res = gazu.log_in(self.login, self.password)
            #print(res)
            self.response.emit(f"Sucessfully login to Kitsu as {res['user']['full_name']}")
            self.kitsu.user_updated.emit( res['user'] )
            self.logged.emit()
        except Exception as e :
            print(e)
            self.response.emit('Fail to log \n')


class FetchThread(QThread) :
    def __init__( self, kitsu, project ) :
        super().__init__()

        self.kitsu = kitsu
        self.project_name = project

    def run(self) :

        project = gazu.project.get_project_by_name(self.project_name)
        self.kitsu.project_updated.emit(project)

        persons = gazu.client.fetch_all(f"persons?project_id={project['id']}")
        self.kitsu.persons_updated.emit(persons)

        task_status = gazu.client.fetch_all("task-status")
        self.kitsu.task_status_updated.emit(task_status)

        task_types = gazu.task.all_task_types()
        self.kitsu.task_types_updated.emit(task_types)

        entity_types = gazu.entity.all_entity_types()
        self.kitsu.entity_types_updated.emit(entity_types)

        sequences = gazu.shot.all_sequences_for_project(project)
        self.kitsu.sequences_updated.emit(sequences)

        assets = gazu.client.fetch_all(f"assets/with-tasks?project_id={project['id']}")
        for a in assets : # get casting
            a['project_id'] = project['id']
            a['casting'] = gazu.casting.get_asset_casting(a)
        self.kitsu.assets_updated.emit(assets)

        shots = gazu.client.fetch_all(f"shots/with-tasks?project_id={project['id']}")
        casting = {s['id'] : gazu.casting.get_sequence_casting(s) for s in sequences}
        for s in shots : # get casting
            s['casting'] = casting[s['parent_id']].get(s['id'], [] )

        self.kitsu.shots_updated.emit(shots)

        #my_tasks = gazu.user.all_tasks_to_do()
        self.kitsu.my_tasks_ready.emit()

        #self.kitsu.logged.emit(f"Sucessfully login to Kitsu")
        self.kitsu.connected.emit()

        print('Fetch Finished')


class Kitsu(QObject) :

    logged = Signal(str)
    connected = Signal()

    user_updated = Signal(dict)
    my_tasks_ready = Signal()

    project_updated = Signal(dict)
    persons_updated = Signal(dict)
    task_status_updated = Signal(list)
    task_types_updated = Signal(list)
    entity_types_updated = Signal(list)
    sequences_updated = Signal(list)
    assets_updated = Signal(list)
    shots_updated = Signal(list)

    def __init__(self):
        super().__init__()

        self.login_thread = None
        self.fetch_thread = None

    def login(self, url=None, login=None, password=None ) :
        if not url :
            url = env['KITSU_URL']
        if not url.endswith('/api') :
            url += '/api'

        login = login if login else env['KITSU_LOGIN']
        password = password if password else env['KITSU_PASSWORD']

        if self.login_thread and self.login_thread.isRunning() :
            self.login_thread.terminate()

        self.login_thread = LoginThread(self, url, login, password)
        self.login_thread.start()

    def fetch(self, project=None) :
        project = project if project else env['KITSU_PROJECT']

        if self.fetch_thread and self.fetch_thread.isRunning() :
            self.fetch_thread.terminate()

        self.fetch_thread = FetchThread(self, project)
        self.fetch_thread.start()

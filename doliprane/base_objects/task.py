
from PySide2.QtWidgets import QApplication
from PySide2.QtCore import QObject, Signal

import template
import sys
import subprocess
import platform
from pathlib import Path
import shutil

from doliprane.windows.open_task_dlg import OpenTaskDlg
from doliprane.base_objects.version import Versions, Version
from doliprane.base_objects.collection import Collection
from doliprane.constants import *


class BaseTask(QObject) :
    status_updated = Signal()
    versions_updated = Signal()

    def __init__(self, entity, data) :
        super().__init__()

        self.project = entity.project

        self.data = data

        self.entity = entity
        self.id = data['id']

        self.status = self.project.task_status.get_by_id(data['task_status_id'])

        assignees = [self.project.persons.get_by_id(a) for a in data['assignees'] ]
        self.assignees = Collection.from_attr('full_name', assignees)

        self.versions = Versions(self.project)
        #self.local_versions = Collection()
        self.widgets = []

        self.extension = self.task_type.extension
        self.name = self.task_type.name
        self.norm_name = self.task_type.norm_name
        self.soft = self.task_type.soft

        self.build_script = self.task_type.build_script
        self.open_script = self.task_type.open_script
        self.pre_publish_script = self.task_type.pre_publish_script
        self.publish_script = self.task_type.publish_script

        #self.local_file_template = self.task_type.local_file_template
        self.tpl_file = self.task_type.tpl_file
        self.tpl_local_file = self.task_type.tpl_local_file

        self.tpl_screenshot = self.task_type.tpl_screenshot

        self.tpl_output = self.task_type.tpl_output
        self.tpl_local_output = self.task_type.tpl_local_output

        self.tpl_infos = self.task_type.tpl_infos

        #self._playblast = self.conf.get('playblast')

        #if self._playblast and self._get_path.parent.suffix == '.py' :
        #    self._playblast = get_func_from_path(self._playblast.parent, self._playblast.stem)

    @property
    def file(self) :
        return Path( self.tpl_file.format(self.format_data) )

    @property
    def screenshot(self) :
        return Path( self.tpl_screenshot.format(self.format_data) )

    def update(self):
        import gazu

        t = gazu.task.get_task(self.id)
        self.status = self.project.task_status.get_by_id(t['task_status_id'])
        self.status_updated.emit()
        pass

    @property
    def work_in_local(self) :
        return APP.preferences.general.work_in_local

    def set_versions(self) :
        #print('data', data)

        local_versions = self.tpl_local_file.find_all(self.format_data, get='version')
        distant_versions = self.tpl_file.find_all(self.format_data, get='version')

        #print('local_versions', local_versions)
        #print('distant_versions', distant_versions)
        versions = list(set(local_versions+distant_versions))

        self.versions.set(self, sorted(versions) )

        self.versions_updated.emit()

    def insert_file(self, file) :
        copy = 'copy' if platform.system() == 'Windows' else 'cp'

        dst = self.tpl_file.find(self.format_data, version=template.LAST_NEXT)
        if not dst :
            dst = self.tpl_file.format(self.format_data, version=1)
        Path(dst).parent.mkdir(parents=True, exist_ok=True) # Create parent dir
        cmd = [copy, str(file), str(dst)]


        subprocess.call(cmd)
        self.set_versions()


    def open(self, version_name) :
        if version_name == 'None' :
            self.build(keep_open=True)
        else :
            active_version = self.task.versions[version_name]
            active_version.open()


    def build(self, background=False, local=None) :
        print(f'Build Task {self.name}')

        python_path = str(MODULE_DIR.parent)
        sys.path.append(python_path)

        if local is None :
            local = self.work_in_local

        if local :
            tpl_file = self.tpl_local_file
        else :
            tpl_file = self.tpl_file

        file = Path(tpl_file.format( self.format_data, version=0 ) )
        file.parent.mkdir(exist_ok=True, parents=True)
        #file.touch()
        src_file = None
        if self.task_type.build_from :
            task_name = APP.project.task_types[self.task_type.build_from].norm_name
            if self.task_type.for_shots :
                format_data = {**self.format_data, **{'shot_task': task_name} }
            else :
                format_data = {**self.format_data, **{'task': task_name} }

            src_file = Path(tpl_file.find( format_data, version=template.LAST ) )

        version = self.task_type.soft_version
        cmd = self.soft.get_cmd(file=src_file, version=version,  python=str(self.build_script),
        background=background, args={'--file':str(file), '--entity' : self.entity.to_str()} )

        print('cmd :', cmd)

        subprocess.Popen(cmd)

        self.set_versions()


    '''
    def get_local_paths(self, version) :
        tpl = self.tpl_local_file
        data = self.get_template_data()
        if version in template.FILTERS :
            return tpl.find_all(data )
        else :
            return tpl.format(data, version=version)

    def get_paths(self, version):
        tpl = self.tpl_file
        data = self.get_template_data()
        if version in template.FILTERS :
            return tpl.find_all(data )
        else :
            return tpl.format(data, version=version)
    '''

class AssetTask(BaseTask) :
    def __init__(self, asset, data) :
        self.project = asset.project
        self.task_type = self.project.asset_task_types.get_by_id(data['task_type_id'])
        super().__init__(asset, data)

    @property
    def format_data(self) :
        return {**self.entity.format_data, 'ext': self.extension }


class ShotTask(BaseTask) :
    def __init__(self, shot, data) :
        self.project = shot.project
        self.task_type = self.project.shot_task_types.get_by_id(data['task_type_id'])
        super().__init__(shot, data)

        #print('SHOT DATA')

    @property
    def format_data(self) :
        return {**self.entity.format_data, 'shot_task': self.norm_name , 'ext': self.extension }


    def to_dict(self) :
        data = dict(
            name = self.name,
            number = self.number,
            casting = []
        )

    '''
    def get_template_data(self) :
        name = self.entity.norm_name

        data = dict( shot=self.number, sequence=self.sequence.number,
        shot_task=name , ext=self.extension )

        return data
    '''

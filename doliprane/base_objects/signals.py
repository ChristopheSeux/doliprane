
from PySide2.QtCore import QObject, Signal


class Signals(QObject) :
    connected = Signal()

    persons_updated = Signal()
    task_types_updated = Signal()
    task_status_updated = Signal()
    assets_updated = Signal()
    sequences_updated = Signal()
    shots_updated = Signal()
    entity_types_updated = Signal()
    my_tasks_updated = Signal()
    closed = Signal()

    project_setted = Signal()

    #new_connection = Signal(object)
    connection_updated = Signal(object)

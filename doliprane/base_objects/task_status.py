
from PySide2.QtGui import QColor

class TaskStatus :
    def __init__(self, project, data) :
        self.project = project
        self.data = data
        self.id = data['id']
        self.name = data['name']
        self.short_name = data['short_name']
        self.norm_name = self.short_name.upper()

        self.is_artist_allowed = data['is_artist_allowed']
        self.is_client_allowed = data['is_client_allowed']
        self.is_done = data['is_done']
        self.is_reviewable = data['is_reviewable']

        color = data['color']
        if color == '#f5f5f5' :
            color = '#7F7F7F'

        self.color = QColor(color)

        #print(data)


import yaml
import re
from doliprane.base_objects.conf import Conf
from doliprane.base_objects.collection import Collection

from doliprane.constants import APP
import subprocess
import json
from PySide2.QtCore import QProcess, QIODevice, QThread


class ActionQueue(QThread) :


    def __init__(self, cmds) :
        super().__init__()

        self.cmds = cmds
        self.processes = []

    def run(self) :

        print('\n-----Run')

        for cmd in self.cmds :
            '''
            process = QProcess()
            process.setProcessChannelMode(QProcess.MergedChannels)
            process.readyReadStandardOutput.connect(lambda : self.process_output(process) )
            process.finished.connect(lambda : self.process_finished(process) )

            self.processes.append(process)
            print('Start Process', process)
            process.start(cmd[0], cmd[1:], QIODevice.ReadWrite)
            process.waitForReadyRead()
            '''
            process = subprocess.call(cmd)
            self.processes.append(process)

        #self.finished.emit()
        print('\n-----Finished Process')

    def process_output(self, process):
        """Exécuté lorsque le processus envoie des infos à afficher.
           La chaine renvoyée par data() est de type byte, terminée
           par une fin de ligne.
           L'encodage dépend de la commande lancée.
        """
        data = process.readAllStandardOutput().data()
        ch = str(data, encoding="utf-8").rstrip()
        print(ch)

    def process_finished(self, process):
        """Exécuté lorsque le processus envoie des infos à afficher.
           La chaine renvoyée par data() est de type byte, terminée
           par une fin de ligne.
           L'encodage dépend de la commande lancée.
        """
        if process!=None:
            # le processus vient de se terminer: on fait le ménage
            process.readyReadStandardOutput.disconnect()
            process.finished.disconnect()
            print("Processus terminé")


class Argument :
    def __init__(self, conf) :
        super().__init__()

        self.name = conf.get('name', '')
        self.default = conf.get('default')
        self.arg = conf.get('arg', '')
        self.type = type(self.default)
        self.data_path = conf.get('data_path', '')
        self.norm_name = self.name.replace(' ', '_').lower()

        if self.arg is None and self.name :
            self.arg = self.name.lower().replace('_', '-').replace(' ', '-')

        elif self.name is None and self.arg :
            self.name = self.arg.strip('-').replace('-', ' ').replace('_', ' ').title()


class Action(Conf) :
    def __init__(self, project, conf) :
        super().__init__(conf)

        self.name = self.conf.get('name')
        self.path = self.conf.get('path')
        self.on_each = self.conf.get('on_each')
        self.focus = self.conf.get('focus', True)

        self.project = project

        soft = self.conf.get('soft','Python')
        self.soft = self.project.softs[soft]
        self.soft_version = conf.get('soft_version', None)

        self.arguments = Collection()
        if conf.get('arguments') :
            arguments = [ Argument(a) for a in self.conf['arguments'] ]
            self.arguments = Collection.from_attr('name', arguments)

    def norm_arg(self, arg_name) :
        arg_name = re.sub('[-]+',' ', arg_name)
        arg_name= arg_name.strip(' ')

        return '--' + arg_name.replace(' ', '-').lower()

    def norm_value(self, value):

        if isinstance(value, (tuple, list)) :
            values = []
            for v in value :
                if not isinstance(v, str):
                    v = json.dumps(v)
                values.append(v)

            return values

        if not isinstance(value, str):
            value = json.dumps(value)
        return value

    def eval_arg(self, data_path) :
        value = eval(data_path)

        if isinstance(value, (tuple, list) ) :
            value = [ json.dumps(v.to_dict() ) for v in value]
        elif not isinstance(value, str):
            value = json.dumps( value.to_dict() )

        return value

    def get_cmds(self, files=[], **kargs) :
        version = self.soft_version

        for arg in self.arguments :
            arg_name = arg.arg or self.norm_arg(arg.name)

            if arg.norm_name in kargs :
                kargs[arg_name] = kargs[arg.norm_name]
                del kargs[arg.norm_name]
            elif arg_name in kargs :
                kargs[arg_name] = kargs[arg_name] # Replace Value a string for the cmdline
            elif arg.data_path :
                kargs[arg_name] = self.eval_arg(arg.data_path)
            else :
                kargs[arg_name] =  arg.default

        norm_args = {}
        for k, v in kargs.items() :
             norm_args[self.norm_arg(k)] = self.norm_value(v)

        cmds= []

        if not files and self.on_each :
            files = [i.to_dict()['path'] for i in eval(self.on_each) ]

        if files :
            for f in files :
                cmds.append( self.soft.get_cmd(version=version, file=f,
                python=str(self.path), args=norm_args, focus=self.focus) )
        else :
            cmds = [ self.soft.get_cmd(version=version,  python=str(self.path),
            args=norm_args, focus=self.focus) ]

        return cmds


    def run(self, files=[], **kargs) :
        cmds = self.get_cmds(files=[], **kargs)

        print(f'Run Action {self.name} with this cmd : {cmds}')
        self.action_queue = ActionQueue(cmds)
        self.action_queue.start()
        #subprocess.Popen(cmd)
        #return self.action_queue


    def to_dict(self) :
        pass

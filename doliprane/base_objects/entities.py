
from PySide2.QtGui import QImage
try :
    from PySide2.QtGui import QColorSpace
except :
    QColorSpace = None
    print('Cannot import QColorSpace QT version <5.12')

from PySide2.QtCore import QObject, Signal, Qt, QSize

from doliprane.base_objects.collection import Collection
from doliprane.base_objects.task import AssetTask, ShotTask
from doliprane.base_objects.collection import Collection
from doliprane.base_objects.action import Action, ActionQueue
from doliprane.utils import copy

import shutil
from pathlib import Path
import json
import os
import gazu
import template


class AssetCasting(QObject) :
    def __init__(self, project, data) :
        super().__init__()

        self.project = project
        self.data = data
        self.nb_occurences = data['nb_occurences']

        self.asset = self.project.assets.get_by_id(data['asset_id'])


class BaseAsset(QObject) : # Remane it entity
    preview_updated = Signal()
    def __init__(self, project, data) :
        super().__init__()

        self.project = project
        self.data = data
        self.id = data['id']
        self.name = data['name']
        self.norm_name = project.templates.norm_str(self.name)
        self.description = data['description']

        conf = self.parent.conf
        #print('project.templates',  project.templates)
        self.tpl_preview = project.templates[ conf['preview_template'] ]
        self.tpl_thumbnail = project.templates[ conf['thumbnail_template'] ]

        self.tpl_output = project.templates[ conf['output_template'] ]
        self.tpl_local_output = project.local_templates[ conf['output_template'] ]

        self.tpl_root = project.templates[ conf['root_template'] ]
        self.tpl_local_root = project.local_templates[ conf['root_template'] ]

        self.tpl_file = project.templates[ conf['file_template'] ]
        self.tpl_screenshot = project.templates[ conf['screenshot_template'] ]

        self.tpl_infos = project.templates[ conf['infos_template'] ]

        self.casting = []

        self.dependencies = []


    def set_dependencies(self) :
        if self.scene_info.exists() :
            scene_infos = json.loads( self.scene_info.read_text() )
            self.dependencies = scene_infos['libraries']

    def set_casting(self) :
        self.casting = [AssetCasting(self.project, a) for a in self.data['casting'] ]

    def get_all_versions(self) :
        return [v for t in self.tasks for v in t.versions]

    def get_dependencies(self, versions=[]) :
        dependencies = set()
        versions = versions or self.get_all_versions()

        file_infos = None
        for v in versions : # Not read json if not necessary
            #if v.file_infos != file_infos :
            v.get_file_infos()
            dependencies |= set( v.dependencies )

            file_infos = v.file_infos

        return list(dependencies)

    @property
    def scene_info(self) :
        return self.root/'scene_info.json'

    @property
    def local_output(self) :
        return Path( self.tpl_local_output.format(self.format_data) )

    @property
    def output(self) :
        return Path( self.tpl_output.format(self.format_data) )

    @property
    def local_root(self) :
        return Path( self.tpl_local_root.format(self.format_data) )

    @property
    def root(self) :
        return Path( self.tpl_root.format(self.format_data) )

    @property
    def thumbnail(self) :
        return Path( self.tpl_thumbnail.format(self.format_data) )

    @property
    def preview(self) :
        return Path( self.tpl_preview.format(self.format_data) )


    def upload(self, versions=[]) :
        self.root.mkdir(exist_ok=True, parents=True)
        #self.set_dependencies()

        #print('###')
        #print(self.dependencies)

        if self.local_root.exists() :
            print(f'Upload version from {self.local_root} to {self.root}')

            excludes = ['old','_old']
            if versions :
                excludes+= [v.path.name for v in self.get_all_versions() if
                v not in versions ]

            copy(self.local_root, self.root, only_new=True, excludes=excludes)

            # Dependencies are relative for now
            for d in self.get_dependencies(versions=versions) :
                if d.path.exists() :
                    copy(d.local_path, d.path, only_new=True)

        else :
            print(f'The directory {self.root} does not exist')


    def remove(self) :
        if self.local_root.exists() :
            print('Remove Local Entity', self.local_root)
            shutil.rmtree( str(self.local_root) , ignore_errors=True)

        if self.root.exists() :
            print('Remove Entity', self.root)
            shutil.rmtree( str(self.root) , ignore_errors=True)


    def download(self, versions=[]) :
        self.local_root.mkdir(exist_ok=True, parents=True)

        if self.root.exists() :
            print(f'Download version from {self.root} to {self.local_root}')

            '''
            excludes = ['old','_old']
            if versions :
                all_versions = [v for t in self.tasks for v in t.versions]
                #print('all_versions', all_versions)
                #exclude_versions = [str(v.path) for v in all_versions if v not in versions]
                #print('exclude_versions', exclude_versions)
                excludes+= [v.path.name for v in all_versions if v not in versions]
            '''
            #print('Excludes', excludes)
            for v in versions :
                copy(v.path, v.local_path, only_new=True)

            # Dependencies
            #self.set_dependencies()
            #print('Dependencies', self.dependencies)
            for d in self.get_dependencies(versions=versions) :
                if d.path.exists() :
                    copy(d.path, d.local_path, only_new=True)


        else :
            print(f'The directory {self.root} does not exist')
        #from distutils.dir_util import copy_tree
        #print(self.local_root, self.root)

        #return
        #self.local_root.mkdir(exist_ok=True, parents=True)
        #copy_tree(str(self.root), str(self.local_root))




    def set_preview(self, image_path) :
        print(f'Set Preview to {self.thumbnail}')

        self.thumbnail.parent.mkdir(exist_ok=True, parents=True)

        size= QSize(150, 100)

        image = QImage( Path(image_path).as_posix() )
        if QColorSpace :
            image.convertToColorSpace(QColorSpace.SRgb)
        image.save(self.preview.as_posix() )

        image = image.scaled(size, Qt.KeepAspectRatioByExpanding, Qt.SmoothTransformation)
        image.save(self.thumbnail.as_posix() )

        self.preview_updated.emit()

        #shutil.copy2(image_path, self.thumbnail)

class Assets(Collection) :
    def __init__(self, project, conf) :
        super().__init__()
        self.project = project
        self.conf = conf

    def set(self, asset_datas) :
        self.clear()
        for asset_data in asset_datas :
            asset = Asset(self.project, asset_data)
            self[asset.norm_name] = asset
        self.project.signals.assets_updated.emit()

    def add(self, asset_data) :
        asset = Asset(self.project, asset_data)
        self[asset.norm_name] = asset

        self.project.signals.assets_updated.emit()

        return asset

    '''
    def get_version_from_path(self, path) :
        try :
            d = self.project.templates['asset_file'].parse(data[1])
            self.entity = self.app.project.assets[d['name']]
        except :
            pass
        return
    '''


class Asset(BaseAsset) :
    def __init__(self, project, data) :
        self.parent = project.assets

        super().__init__(project, data)

        tasks = [ AssetTask(self, t) for t in data['tasks'] ]
        tasks.sort(key=lambda x : x.task_type.priority)
        #print(data['tasks'])
        self.tasks = Collection.from_attr('norm_name', tasks)
        self.type =  project.asset_types.get_by_id( data['asset_type_id'] )

        #print(self.name, conf)
        self.tpl_moodboard = project.templates[ self.parent.conf['moodboard_template'] ]

        self.label = self.type.name+' / '+self.name

    @property
    def moodbard(self) :
        return Path( self.tpl_moodboard.format(self.format_data) )

    @property
    def format_data(self) :
        name = self.norm_name.split('/')[0]
        entity_type = self.type.norm_name
        return dict(name=name, asset_type=entity_type)

    def to_dict(self) :
        data = dict(
            name = self.name,
            norm_name = self.norm_name,
            description = self.description
        )
        return data

    def to_str(self) :
        return json.dumps(self.to_dict(), separators=(',',':'))


    def remove(self) :
        super().remove()

        # Remove asset from kitsu
        gazu.asset.remove_asset(self.data, force=True)


class Shots(Collection) :
    def __init__(self, project, conf) :
        super().__init__()

        self.project = project
        self.conf = conf

        actions = [ Action(project, a) for a in conf.get('actions', []) ]
        self.actions = Collection.from_attr('name', actions)

        for a in actions :
            print(a.name)

    def publish(self) :
        pass

    def play(self, shots) :
        if self.actions.get('Play') :
            print('Play Shots')

            self.actions['Play'].run(shots=shots)
        else :
            print('The action Play is not define')

    def playblast(self, versions, add_to_kitsu=False, burn_stamps=True,
    update_libraries=False, play_on_finish=False) :

        if not self.actions.get('Playblast') :
            print('The action Playblast is not define')
            return

        actions = []

        for v in versions :
            v.playblast(
                add_to_kitsu = add_to_kitsu,
                burn_stamps = burn_stamps,
                update_libraries = update_libraries,
                play_on_finish = play_on_finish
            )

        #action_queue = ActionQueue( actions )
        #action_queue.run()

    def set(self, shots) :
        self.clear()
        for s in shots :
            shot = Shot(self.project, s)
            self[shot.name] = shot
        self.project.signals.shots_updated.emit()

    def add(self, shot) :
        shot = Shot(self.project, shot)
        self[shot.name] = shot

        self.project.signals.shots_updated.emit()

        return shot

class Shot(BaseAsset):
    def __init__(self, project, data) :
        self.parent = project.shots

        super().__init__(project, data)

        #print(data)

        #raise('Stop')

        #import yaml
        #print('Shot Data')
        #print(yaml.dump(data))
        self.frame_in = data.get('frame_in', None)
        self.frame_out =  data.get('frame_out', None)

        self.type =  project.entity_types.get_by_id( data['entity_type_id'] )

        name_data = project.templates['shot_name'].parse(self.name)
        self.number = name_data.get('shot', -1)

        self.sequence = self.project.sequences.get_by_id(data['sequence_id'])
        self.sequence_number = self.sequence.number

        tasks = [ShotTask(self, t) for t in data['tasks'] ]
        tasks.sort(key=lambda x : x.task_type.priority)
        self.tasks = Collection.from_attr('norm_name', tasks)

        self.label = self.sequence.name+' / '+self.name

    @property
    def format_data(self) :
        data = dict( shot=self.number, sequence=self.sequence.number)
        return data

    def to_dict(self) :
        data = dict(
            **self.format_data,
            name = self.name,
            number = self.number,
            frame_in = self.frame_in,
            frame_out = self.frame_out
        )
        return data

    def to_str(self) :
        return json.dumps(self.to_dict(), separators=(',',':'))


    def remove(self) :
        super().remove()

        # Remove asset from kitsu
        gazu.shot.remove_shot(self.data, force=True)


class Sequences(Collection) :
    def __init__(self, project, conf) :
        super().__init__()
        self.project = project
        self.conf = conf


    def set(self, sequences) :
        self.clear()
        #print('Set Sequences')
        for s in sequences :
            #print(s)
            sequence = Sequence(self.project, s)
            self[sequence.name] = sequence
        self.project.signals.sequences_updated.emit()

    def add(self, sequence) :
        sequence = Sequence(self.project, sequence)
        self[sequence.name] = sequence

        self.project.signals.sequences_updated.emit()

        return sequence

class Sequence(BaseAsset):
    def __init__(self, project, data) :
        self.parent = project.sequences

        super().__init__(project, data)

        self.type =  project.entity_types.get_by_id(data['entity_type_id'] )
        self.name = data['name']
        #print('Sequence Name', self.name)
        name_data = project.templates['sequence_name'].parse(self.name)
        self.number = name_data.get('sequence', -1)

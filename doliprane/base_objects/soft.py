
from doliprane.base_objects.conf import Conf
import subprocess
import sys
import os
from pathlib import Path
import shutil
import tempfile

import template
from template import Template


from doliprane.base_objects.collection import Collection
from doliprane.constants import APP
from doliprane.utils import copy, unzip


class SoftVersion :
    def __init__(self) :
        self.path = ''
        self.is_installed = False
        self.local_path = False
        self.archive_path = ''



class Soft(Conf) :
    def __init__(self, project, conf) :
        super().__init__(conf)

        #print(self.conf)

        self.name = self.conf.get('name', 'default')
        self.install_script = self.conf.get('install')
        self.icon = self.conf.get('icon', '')
        self.hide = self.conf.get('hide', False)

        self.extension = self.conf.get('extension', '.ext')

        self.project = project
        self.softs_dir = project.softs_dir

        path = self.conf.get('path')
        if path and path.parent.suffix == '.py' :
            self._get_path = self._get_func_from_path(path.parent, path.stem)
        else :
            self._get_path = lambda **kwargs : str(path)
        #print(self.name)
        #print('self._get_path',self._get_path)

        self._get_cmd = self.conf.get('cmd')
        if self._get_cmd and self._get_cmd.parent.suffix == '.py' :
            self._get_cmd = self._get_func_from_path(self._get_cmd.parent, self._get_cmd.stem)

        self.versions = self.conf.get('versions', [])
        self.tpl_version = None
        if conf.get('version_template') :
            SOFTS_DIR = Path(os.getenv('SOFTS_DIR')).as_posix()
            self.tpl_version = Template( f"{SOFTS_DIR}/{conf['version_template']}" )
            #print( self.tpl_version.find_all({} ) )
            #print(self.tpl_version)
            #print(self.tpl_version.reg)
            all_versions = self.tpl_version.find_all({} )
            #print('\n', all_versions, '\n')
            self.versions = [self.tpl_version.parse(v)['version'] for v in all_versions]

        self.active_version = self.versions[0] if self.versions else None

    def _get_func_from_path(self, path, func_name) :
        from importlib import util

        spec = util.spec_from_file_location(func_name, path)
        mod = util.module_from_spec(spec)

        spec.loader.exec_module(mod)

        return getattr(mod, func_name)

    def get_path(self, version=None) :
        if not version :
            version = self.active_version
            print(f'Choose default version ({version}) for {self.name}')

        return self._get_path(version=version, softs_dir=str(self.softs_dir) )

    def get_cmd(self, version=None, file=None, python=False, args={}, background=False, focus=True) :
        if isinstance(file, (list, tuple)) :
            file = [str(f) for f in file]
        elif file is not None:
            file=str(file)

        #print('###SOFT DIR', os.environ['SOFTS_DIR'])
        soft_path = self.get_path(version)
        if self._get_cmd :
            cmd = self._get_cmd(soft_path, file=file,
            python=python, args=args, background=background, focus=focus)
        else :
            cmd = [soft_path]

            if file :
                cmd +=[str(file)]
            elif python :
                cmd +=[str(python)]

            for k, v in args.items():
                cmd+=[k]

                if isinstance(v, (tuple, list)) :
                    cmd+= v
                else :
                    cmd+=[v]

        return cmd

    def launch(self, version=None, file=None, env={}) :
        print('Launch', self.name, version)
        #print('###SOFT DIR', os.environ['SOFTS_DIR'])
        cmd = self.get_cmd(version=version, file=file, background=False)
        #print(cmd)
        subprocess.Popen(cmd, env={**os.environ,**env})

    def install(self, version=None) :
        print('Install ', self.name)
        version = version or self.active_version

        if self.install_script :
            python = sys.executable
            subprocess.Popen([python, str(self.install_script), '--version', version])
        else : #Default install copy, unzip and rename
            #To do Check if soft already installed
            all_softs = set( self.softs_dir.glob('*') )

            src = Path( self.tpl_version.find({'version':version}) )
            dst = self.softs_dir / src.name

            print(f'Copy from {src} to {dst}')
            copy(src, dst)

            #extract_dir = Path(tempfile.tmpdir() ) / src.name
            print(f'Unzip {self.name}')
            shutil.unpack_archive(dst, self.softs_dir)
            #unzip(dst, self.softs_dir)

            print('Remove Zip')
            os.remove(dst)

            print('Rename')
            new_softs = list( set( self.softs_dir.glob('*') ) - all_softs )
            print( list(new_softs) )

            if len(new_softs) == 1 :
                src = new_softs[0]
                dst = new_softs[0].with_name(f'{self.name.lower()}-{version}')

                print('Move', src, 'to', dst)

                shutil.move(str(src), str(dst) )

            '''
            copy(src, dst)

            temp_dir = Path(tempfile.gettempdir() )
            filename = Path('/mnt/s/softs/linux/blender-2.83.3.tar.xz')
            softs_dir = Path('/home/graph/Documents/ACS/softs')

            extract_dir = Path(tempfile.gettempdir() ) / filename.name
            #shutil.unpack_archive(filename, extract_dir)

            print()

            #print(extract_dir.glob('*'))


            list_files = list( extract_dir.glob('*') )
            if len(list_files) == 1 and list_files[0].is_dir() :
                print('Oui')
            '''

    def uninstall(self) :
        print('UnInstall ', self.name)
        version = self.widget.versions_enum.value
        soft_root = APP.project.softs_dir/f'{self.name}-{version}'
        shutil.rmtree(soft_root)

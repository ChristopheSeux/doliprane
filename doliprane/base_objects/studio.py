

class Studio(Conf) :
    def __init__(self, conf) :
        super().__init__(conf)

        app = QApplication.instance()

        self.signals = app.signals
        self.kitsu = app.kitsu

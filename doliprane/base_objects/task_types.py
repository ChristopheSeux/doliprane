
from PySide2.QtWidgets import QApplication

from pathlib import Path
import sys
import subprocess
import template

from doliprane.constants import *
from doliprane.base_objects.conf import Conf
from doliprane.base_objects.collection import Collection


class TaskType(Conf) :

    def __init__(self, task_types, data, conf) :
        super().__init__(conf)

        signals = QApplication.instance().signals

        self.project = task_types.project
        self.data = data
        self.conf = conf

        self.name = data['name']
        self.norm_name = self.project.templates.norm_str(self.name)
        self.for_shots = data['for_shots']
        self.color = data['color']

        self.build_from = conf.get('build_from')

        self.id = data['id']
        self.task_types = task_types
        self.priority = data['priority']

        self.build_script = conf.get('build')
        self.open_script = conf.get('open')
        self.pre_publish_script = conf.get('pre_publish')
        self.publish_script = conf.get('publish')

        soft = conf.get('soft','default')
        self.soft = self.project.softs[soft]
        self.soft_version = conf.get('soft_version', None)
        self.extension = self.soft.extension
        self.work_in_sequence = conf.get('work_in_sequence', False)

        #self.versions = []


        self.tpl_thumbnail_name = conf.get('thumbnail_template', self.task_types.tpl_thumbnail_name)
        self.tpl_thumbnail = self.project.templates[ self.tpl_thumbnail_name ]

        self.tpl_file_name = conf.get('file_template', self.task_types.tpl_file_name)
        self.tpl_file = self.project.templates[ self.tpl_file_name ]

        self.tpl_local_file = self.task_types.tpl_local_file

        self.tpl_screenshot = self.task_types.tpl_screenshot
        #signals.task_types_updated.connect(self.set_data)
        self.tpl_output_name = conf.get('output_template', self.task_types.tpl_output_name)
        self.tpl_output = self.project.templates[ self.tpl_output_name ]
        self.tpl_local_output = self.task_types.tpl_local_output

        self.tpl_infos_name = conf.get('infos_template', self.task_types.tpl_infos_name)
        self.tpl_infos = self.project.templates[ self.tpl_infos_name ]

    '''
    def set_data(self) :
        task_types = QApplication.instance().kitsu.task_types
        self.data = task_types[self.name]
        self.id = self.data['id']


    def get_versions(self, data) :
        tpl = self.project.templates

        all_versions_files = self.get_file_path(data, version=template.ALL)

        return [ tpl[self.file_template].parse(f) for f in all_versions_files ]
    '''


class ShotTaskType(TaskType) :
    type = 'shot'
    def __init__(self, task_types, data, conf) :
        super().__init__(task_types, data, conf)


class AssetTaskType(TaskType) :
    type = 'asset'
    def __init__(self, task_types, data, conf) :
        super().__init__(task_types, data, conf)


class TaskTypes(Collection) :
    def __init__(self, project, datas, conf) :
        super().__init__()

        self.project = project

        self.tpl_thumbnail_name = conf['thumbnail_template']
        self.tpl_thumbnail = project.templates[ self.tpl_thumbnail_name ]

        self.tpl_file_name = conf['file_template']
        self.tpl_file = project.templates[ self.tpl_file_name ]
        self.tpl_local_file = self.project.local_templates[ self.tpl_file_name ]

        self.tpl_screenshot_name = conf['screenshot_template']
        self.tpl_screenshot = project.templates[ self.tpl_screenshot_name ]

        self.tpl_output_name = conf['output_template']
        self.tpl_output = project.templates[ self.tpl_output_name ]
        self.tpl_local_output = self.project.local_templates[ self.tpl_output_name ]

        self.tpl_infos_name = conf['infos_template'] # Has to make it not obligatory
        self.tpl_infos = project.templates[ self.tpl_infos_name ]



class AssetTaskTypes(TaskTypes) :
    def __init__(self, project, datas, conf) :
        super().__init__(project, datas, conf)

        #print(datas)

        for data in datas :
            task_name = data['name']
            task_conf = {t['name']:t for t in conf['tasks']}.get(task_name, {})

            if not task_conf :
                print(f'The task {task_name} is not configured')

            self[task_name] = AssetTaskType(self, data, task_conf)


class ShotTaskTypes(TaskTypes) :
    def __init__(self, project, datas, conf) :
        super().__init__(project, datas, conf)

        for data in datas :
            task_name = data['name']
            task_conf = {t['name']:t for t in conf['tasks']}.get(task_name, {})

            if not task_conf :
                print(f'The task {task_name} is not configured')

            self[task_name] = ShotTaskType(self, data, task_conf)

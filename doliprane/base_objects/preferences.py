
from PySide2.QtCore import Signal

from doliprane.widgets.properties import *


class PropsGroup() :
    def __init__(self, **kargs) :
        super().__init__()
        for k,v in kargs.items() :
            super().__setattr__(k, v)

    def __getattribute__(self, k) :
        if hasattr(super().__getattribute__(k), 'value') :
            return super().__getattribute__(k).value
        else :
            return super().__getattribute__(k)

    def __setattr__(self, k, v) :
        if hasattr(v, 'value') or isinstance(v, PropsGroup) :
            super().__setattr__(k, v)
        else :
            super().__getattribute__(k).value = v

    def __getitem__(self, k):
        return super().__getattribute__(k)

    def __setitem__(self, key, value):
        self._data[key].value = value

    def items(self) :
        return self.__dict__.items()

    def keys(self) :
        return self.__dict__.keys()

    def values(self) :
        return self.__dict__.values()

    def to_dict(self) :
        return self.__dict__

    def dict(self) :
        return {k:getattr(self,k) for k in self.keys()}


class PreferenceProps(PropsGroup) :
    def __init__(self) :
        super().__init__()


        self.general = PropsGroup(
            work_in_local = Bool(value=True, name='Work in Local'),
            local_root = Filepath(value='', file_mode='Directory'),
            display_thumbnails = Bool(value=False, name='Display Thumbnails')
        )

        self.display = PropsGroup(
            ui_scale = Float(value=1.0, name='UI Scale', min=0.5, max=5, step=0.05),
            brightness = Float(value=0.5, name='Brightness')
        )

        self.theme = PropsGroup(
            background_color = Color(34, 36, 39, name='Background Color'),
            widget_base_color = Color(78, 81, 89, name='Widget Base Color'),
            widget_dark_color = Color(54, 57, 63, name='Widget Dark Color'),
            widget_very_dark_color = Color(44, 46, 51, name='Widget Very Dark Color'),
            widget_bright_color = Color(110, 115, 118, name='Widget Base Color'),
            text_color_disabled = Color(170, 170, 170, name='Text Color Disabled'),
            text_color_normal = Color(190, 190, 190, name='Text Color Normal'),
            text_color_highlight = Color(240, 240, 240, name='Text Color Highlight'),
            select_text_bg = Color(30, 35, 40, name='Selected Text Background'),
            border_radius = Int(1, name='Border Radius',min=0, max=10, decimals=1, unit='px'),
            border_width = Int(1, name = 'Border Width',min=0, max=10, decimals=0, unit='px'),
            hover_color = Color(94, 106, 144, name='Hover Color'),
            highlight_color = Color(85, 127, 193, name='Highlight Color'),
        )

        '''
        # Set Fonts
        font_db = QFontDatabase()
        for f in os.scandir(FONT_PATH) :
            font_id = font_db.addApplicationFont(f.path)
            family = QFontDatabase.applicationFontFamilies(font_id)
            print(f.name, family)
        '''

#from PySide2.QtCore import *
#from PySide2.QtGui import *
from PySide2.QtWidgets import QApplication, QSizePolicy

from doliprane.widgets.basic_widgets import *
from doliprane.widgets.properties import *

from doliprane.constants import APP

import os

class SoftWidget(HWidget) :
    def __init__(self, soft, parent=None):
        super().__init__(parent=parent, Spacing='small')

        self.soft = soft
        self.soft.widget=self

        #self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Maximum)
        self.setFixedWidth(215)

        self.launch_btn = PushButton(parent=self, icon=soft.icon,
        Width='huge', Height='huge', IconSize='very_big',
        connect=self.on_launch_clicked )

        self.version_widget = VWidget(parent=self)

        label = TextLabel(parent=self.version_widget, text=soft.name)

        self.version_row = HWidget(parent=self.version_widget)
        self.versions_enum = Enum(parent=self.version_row, items=soft.versions)
        self.versions_enum.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.versions_enum.value_changed.connect(self.set_active_version)

        self.install_btn = PushButton(parent=self.version_row, icon='install_32',
        IconSize='small', cursor='PointingHandCursor',
        connect=soft.install)

        #Frame(self.version_row, tag='small')
        #self.uninstall_btn = PushButton(parent=self.version_row, icon='close_32',
        #IconSize='small', cursor='PointingHandCursor',
        #connect=soft.uninstall)

        #self.label_widget.layout.addStretch()
        self.restore_settings()
        APP.signals.closed.connect(self.save_settings)

    def set_active_version(self, version) :
        self.soft.active_version = version

        soft_name = self.soft.name.upper().replace(' ', '_')
        APP.project.env[soft_name+'_VERSION'] = version

        #print(soft_name+'_VERSION', version)

    def on_launch_clicked(self, modifiers) :
        self.soft.launch(version=self.versions_enum.value)


    def restore_settings(self):
        soft_version = APP.project.settings.value(self.soft.name, None)

        if soft_version :
            self.versions_enum.value = soft_version


    def save_settings(self):
        APP.project.settings.setValue(self.soft.name, self.versions_enum.value )

class Softs(VWidget) :
    def __init__(self, parent=None):
        super().__init__(parent, Background='dark', Padding='big', Spacing='normal')

        self.setWindowTitle('Softs')

        self.softs_widgets = []




    def set_softs(self) :
        project = QApplication.instance().project
        #print('Set Softs for projects', project.name)

        self.layout.clear()
        for w in self.softs_widgets :
            w.setParent(None)
        #self.layout = VLayout(self) #self.layout.clear()

        for soft in project.softs :
            if soft.name == 'default' or soft.hide : continue

            soft_widget = SoftWidget(parent=self.layout, soft=soft)

            self.softs_widgets.append(soft_widget)

        self.setLayout(self.layout)
        self.layout.addStretch()

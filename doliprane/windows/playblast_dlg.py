
from doliprane.widgets.properties import Bool
from doliprane.widgets.basic_widgets import VDialog, FWidget, PushButton
from doliprane.constants import APP
from PySide2.QtCore import Qt

class PlayblastDlg(VDialog) :
    def __init__(self, parent=None, connect=None):
        super().__init__(parent, title='Playblast', Spacing='small', Padding='normal')

        self.resize(*APP.resize(240, 0) )

        #form_widget = FWidget(self, Spacing='small')

        self.update_libraries = Bool(parent=self, name='Update Libraries')
        self.update_libraries.setFocusPolicy( Qt.NoFocus )

        self.burn_stamps = Bool(True, parent=self, name='Burn Stamps')
        self.burn_stamps.setFocusPolicy( Qt.NoFocus )

        #form_widget.layout.row('Update Libraries:', self.update_libraries)

        self.add_to_kitsu = Bool(parent=self, name='Add To Kitsu')
        self.add_to_kitsu.setFocusPolicy( Qt.NoFocus )
        #form_widget.layout.row('Add To Kitsu:', self.add_to_kitsu)

        self.play_on_finish = Bool(parent=self, name='Play On Finish')
        self.play_on_finish.setFocusPolicy( Qt.NoFocus )
        #form_widget.layout.row('Play On Finish:', self.play_on_finish)

        self.ok = PushButton(self, text= 'OK')

        if connect :
            self.ok.clicked.connect(connect)

        self.ok.clicked.connect(self.close)

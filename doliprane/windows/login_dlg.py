
from PySide2.QtCore import Qt
#from PySide2.QtGui import *
from PySide2.QtWidgets import QApplication

from doliprane.widgets.properties import *
from doliprane.widgets.basic_widgets import *
from doliprane.constants import *
#from doliprane.utils import *

import gazu
import os


class LoginDlg(VDialog) :
    def __init__(self, parent=None):
        super().__init__(parent, title='Login')

        self.resize(*APP.resize(360, 0) )
        self.log_thread = None

        #self.label = TextLabel(self, 'KITSU', align='Center',
        #Background='normal', Height='big', FontSize='big', FontColor='disabled')

        form_widget = FWidget(self, Spacing='small', Padding='normal')

        #self.form_layout = FormLayout(widget.layout, spacing='small')

        self.url = LineEdit()
        form_widget.layout.row('Url', self.url)

        self.login = LineEdit()
        form_widget.layout.row('Login', self.login)

        self.password = LineEdit()
        self.password.setEchoMode(QLineEdit.Password)
        form_widget.layout.row('Password', self.password)

        self.connect = PushButton(self, text='Connect', Hmargin='normal' )
        self.connect.clicked.connect(self.connect_to_kitsu)

        self.layout.addWidget(Frame(tag='small') )

        self.console = ListLabel(self, align='Left', Background='very_dark')
        self.console.widget.layout.setAlignment(Qt.AlignBottom)

        self.restore_settings()
        APP.signals.closed.connect(self.save_settings)

    def restore_settings(self):
        url = APP.settings.value('url', None) or os.environ.get('KITSU_URL', '')
        login = APP.settings.value('login', None) or os.environ.get('KITSU_LOGIN', '')
        password = APP.settings.value('password', None) or os.environ.get('KITSU_PASSWORD', '')
        #print('URL',url )
        #print('env url', os.environ.get('KITSU_URL', '') )
        self.url.setText(url)
        self.login.setText(login )
        self.password.setText(password )

    def save_settings(self):
        APP.settings.setValue('url', self.url.text() )
        APP.settings.setValue('login', self.login.text() )
        APP.settings.setValue('password', self.password.text() )

    def write_to_console(self, text='') :
        self.console.addItem('>> '+ text)
        self.console.updateGeometry()
        scoll_bar = self.console.verticalScrollBar()
        APP.processEvents( )
        scoll_bar.setValue(scoll_bar.maximum())

    def connect_to_kitsu(self) :
        url = self.url.text()
        login = self.login.text()
        password = self.password.text()

        APP.project.env['KITSU_URL'] = url
        APP.project.env['KITSU_LOGIN'] = login
        APP.project.env['KITSU_PASSWORD'] = password

        kitsu = APP.kitsu
        kitsu.login(url, login, password)
        kitsu.login_thread.response.connect(self.write_to_console)
        kitsu.login_thread.logged.connect( kitsu.fetch )

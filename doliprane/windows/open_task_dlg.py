
from PySide2.QtWidgets import QApplication, QButtonGroup, QCheckBox
from PySide2.QtCore import Signal

from doliprane.widgets.basic_widgets import VDialog, TextLabel, PushButton, CheckBox
from doliprane.widgets.properties import Bool


class OpenTaskDlg(VDialog):
    valided = Signal(str)
    def __init__(self, local_version, network_version, parent=None, connect=None):
        super().__init__(parent, title='Warning', Background='dark',
        Padding='normal', Spacing='normal')

        self.app = QApplication.instance()
        self.resize(*self.app.resize(320, 5) )

        self.label = TextLabel(self, text=f'A newer version {network_version} is available on the network:')

        if local_version :
            self.open_local_version = CheckBox(self, text=f'Open local version {local_version}')
        else :
            self.open_local_version = CheckBox(self, text=f'Build version')

        self.copy_btn = CheckBox(self, text=f'Copy network version {network_version} and open it')

        self.button_group = QButtonGroup()

        self.button_group.addButton(self.open_local_version)
        self.button_group.addButton(self.copy_btn)

        self.copy_btn.setChecked(True)

        if connect :
            self.valided.connect(connect)

        self.ok = PushButton(self, text='OK', connect=self.valid)

    def valid(self) :
        self.valided.emit( self.button_group.checkedButton().text() )
        self.close()

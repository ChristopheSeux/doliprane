from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from doliprane.widgets.basic_widgets import *

class Player(VWidget) :
    def __init__(self, parent=None):
        super().__init__()

        self.setWindowTitle('Player')
        self.setProperty('Background', 'dark')


from PySide2.QtCore import Qt
from PySide2.QtWidgets import QApplication, QSizePolicy, QAbstractItemView
#from PySide2.QtGui import *

from doliprane.widgets.properties import *
from doliprane.widgets.basic_widgets import *
from doliprane.constants import APP
#from doliprane.utils import *

from pathlib import Path
import os
import csv
import gazu



def read_csv(path, required_fields, optional_fields) :
    with open(path, 'rU') as csv_file:
        doc = list(csv.reader(csv_file, delimiter=",", quotechar='"') )

    datas = []
    for i, row in enumerate(doc) :
        header_row = [e.lower().strip().replace(' ','_') for e in row]
        #print(header_row)
        if not all([f in header_row for f in required_fields]) : # If all fields are in the row
            continue

        # find the index of the header in the row
        fields = []
        for f, t in required_fields.items() :
            fields.append({'name':f, 'index':header_row.index(f), 'type': t})
        for f, t in optional_fields.items() :
            if f not in header_row : continue
            fields.append({'name':f, 'index':header_row.index(f), 'type': t})

        for r in doc[i+1:] :
            data = {}
            for field in fields :
                name = field['name']
                index = field['index']
                value = r[index]

                if field['type'] is int :
                    value = int(value)
                elif field['type'] is list :
                    value = [v.strip() for v in value.split(',') if v not in ('',' ', '  ')]


                data[name] = value

            datas.append(data)

        return datas

    if not datas :
        print('No Header find with the key Sequence and Shot')
        return []

class ShotWidget(VWidget) :
    def __init__(self, parent=None):
        super().__init__(parent, Spacing='small')

        sq_jump = 10
        sh_jump = 10

        ###### Shot Layout
        self.source = ListEnum(items=['Range', 'Csv'] )
        self.source.value_changed.connect(self.set_settings_lyt)
        self.layout.row('From', self.source)

        self.settings_lyt = StackedLayout(self.layout)
        ### Shot Range
        self.range_wgt = VWidget(self.settings_lyt, Spacing='small')

        self.sequence = Int(sq_jump, min=0, max=1000, decimals=0)
        self.sequence.value_changed.connect(self.set_range_tpl)
        self.range_wgt.layout.row('Sequence',self.sequence)

        self.start = Int(sh_jump, min=0, max=1000, decimals=0)
        self.start.value_changed.connect(self.set_range_tpl)
        self.range_wgt.layout.row('Start',self.start)

        self.jump = Int(sh_jump, min=1, max=1000, decimals=0)
        self.jump.value_changed.connect(self.set_range_tpl)
        self.range_wgt.layout.row('Jump',self.jump)

        self.how_many = Int(min=1, max=1000, decimals=0)
        self.how_many.value_changed.connect(self.set_range_tpl)
        self.range_wgt.layout.row('How Many',self.how_many)

        self.range_wgt.tpl_list = ListLabel(self.range_wgt)

        ### Shot Csv
        self.csv_wgt = VWidget(self.settings_lyt, Spacing='small')
        self.csv = Filepath()
        self.csv.line_edit.textChanged.connect(self.set_csv_tpl)
        self.csv_wgt.layout.row('Path', self.csv)

        self.csv_wgt.tpl_list = ListLabel(self.csv_wgt)

        #self.set_range_tpl()

    def set_csv_tpl(self, path) :
        self.csv_wgt.tpl_list.clear()
        templates = APP.project.templates

        required_fields = {
            'sequence' : int,
            'shot' : int
        }
        optional_fields = {
            'assignees' : list
        }

        shots_data = read_csv(path, required_fields, optional_fields)

        for data in shots_data :
            data = templates.norm_types(data)


            path  = Path(templates['shot_dir'].format(data) )
            #display_path = Path(path).relative_to(os.environ['STORE'])

            i = self.csv_wgt.tpl_list.addItem(path.as_posix())
            i.path = path
            i.data = data
            i.name = templates['shot_name'].format({'shot' : data['shot']})
            i.sequence = templates['sequence_name'].format({'sequence' : data['sequence']})
            i.assignees = data.get('assignees')


    def set_range_tpl(self) :
        self.range_wgt.tpl_list.clear()
        templates = APP.project.templates

        for i in range(self.how_many.value) :
            shot_nb = i + self.start.value + (i*(self.jump.value-1) )
            data = dict(sequence=self.sequence.value, shot=shot_nb)
            path  = Path(templates['shot_dir'].format(data) )
            #display_path = Path(path).relative_to(os.environ['STORE'])

            i = self.range_wgt.tpl_list.addItem(path.as_posix())
            i.path = path
            i.data = data
            i.name = templates['shot_name'].format({'shot' : data['shot']})
            i.sequence = templates['sequence_name'].format({'sequence' : data['sequence']})

    def set_settings_lyt(self, value) :
        if value == 'Range' :
            self.settings_lyt.setCurrentWidget(self.range_wgt)
        elif value == 'Csv' :
            self.settings_lyt.setCurrentWidget(self.csv_wgt)

    def add(self, add_to_kitsu=True, make_dirs=True):
        tpl_list = self.settings_lyt.currentWidget().tpl_list

        p = APP.project
        template = p.templates['shot_file']

        #print(APP.kitsu.task_types)
        #task_types = [t for _,t in APP.kitsu.task_types.items() if t['for_shots']]

        for i in tpl_list.items :
            if add_to_kitsu :
                if i.sequence in p.sequences.keys() :
                    seq = p.sequences[i.sequence]
                else :
                    kitsu_seq = gazu.shot.new_sequence(p.id, i.sequence) #create a new sequence
                    seq = p.sequences.add(kitsu_seq)

                kitsu_shot = gazu.shot.new_shot(p.id, seq.id, i.name )
                kitsu_shot['sequence_id'] = seq.id

                # Create Tasks
                tasks = [gazu.task.new_task(kitsu_shot, t.id ) for t in p.shot_task_types]
                kitsu_shot['tasks'] = tasks

                shot = p.shots.add(kitsu_shot)

            if make_dirs :
                dirs = shot.tpl_file.make_dirs(i.data)
                print('Make dir', dirs)

        p.signals.sequences_updated.emit()
        p.signals.shots_updated.emit()


class AssetWidget(VWidget) :
    def __init__(self, parent=None):
        super().__init__(parent, Spacing='small')


        ###### Asset Layout
        self.source = ListEnum(items=['Name', 'Csv'] )
        self.source.value_changed.connect(self.set_settings_lyt)
        self.layout.row('From', self.source)

        self.settings_lyt = StackedLayout(self.layout)

        ### Asset Name layout
        self.name_wgt = VWidget(self.settings_lyt, Spacing='small')

        self.asset_type = Enum()
        self.asset_type.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.asset_type.value_changed.connect(self.set_name_tpl)
        APP.signals.entity_types_updated.connect(self.set_asset_types)

        self.name_wgt.layout.row('Type',self.asset_type)

        self.name = LineEdit()
        self.name.textEdited.connect(self.set_name_tpl)
        self.name_wgt.layout.row('Name', self.name)

        self.assign_tasks = ListEnum( )
        self.assign_tasks.setSelectionMode(QAbstractItemView.ExtendedSelection)
        APP.signals.task_types_updated.connect(self.set_assign_tasks)
        self.name_wgt.layout.row('Assign', self.assign_tasks)


        self.name_wgt.tpl_list = ListLabel(self.name_wgt)

        ### Asset Csv
        self.csv_wgt = VWidget(self.settings_lyt, Spacing='small')
        self.csv = Filepath()
        self.csv.line_edit.textChanged.connect(self.set_csv_tpl)
        self.csv_wgt.layout.row('Path', self.csv)

        self.csv_wgt.tpl_list = ListLabel(self.csv_wgt)


    def set_assign_tasks(self):
        self.assign_tasks.clear()
        #print(dir(APP.project))
        for t in APP.project.asset_task_types :
            self.assign_tasks.addItem(t.name)

    def set_asset_types(self) :
        self.asset_type.clear()
        for t in APP.project.asset_types :
            self.asset_type.addItem(t.name)

    def set_csv_tpl(self, path) :
        templates = APP.project.templates
        self.csv_wgt.tpl_list.clear()

        required_fields = {
            'name' : str,
            'type' : str
        }
        optional_fields = {
            'description' : str,
            'set_casting' : list,
            'sequence_casting' : list,
            'shot_casting' : list
        }

        asset_data = read_csv(path, required_fields, optional_fields)

        '''
        for a in asset_data :
            for k,v in a.items() :
                print(k, v)
            print()
            '''

        for data in asset_data :
            data = templates.norm_types(data)
            data['name'] = data['name'].replace('-', ' ')
            data['name'] = '-'.join(data['name'].split() )
            data['name'] = templates.norm_str(data['name'])
            format_data = {'asset_type': data['type'], 'name' : data['name']}
            path  = Path(templates['asset_dir'].format(format_data) )
            #display_path = Path(path).relative_to(os.environ['STORE'])

            i = self.csv_wgt.tpl_list.addItem(path.as_posix())
            i.path = path
            i.data = format_data
            i.name = data['name']
            i.asset_type =  templates.norm_str(data['type'])
            i.description = data.get('description', '')
            i.set_casting = data.get('set_casting', [] )
            i.sequence_casting = data.get('sequence_casting', [] )
            i.shot_casting = data.get('shot_casting', [] )

    def set_name_tpl(self) :
        self.name_wgt.tpl_list.clear()
        templates = APP.project.templates

        asset_name = templates.norm_str(self.name.text() )

        if not all([asset_name,self.asset_type.value]) :  return

        data = dict(name=asset_name, asset_type=self.asset_type.value.lower())
        path = Path(templates['asset_dir'].format(data) )
        #display_path = Path(path).relative_to(os.environ['STORE'])

        i = self.name_wgt.tpl_list.addItem(path.as_posix())
        i.path = path
        i.data = data
        i.name = data['name']
        i.asset_type = data['asset_type']
        i.description = ''

    def set_settings_lyt(self, value) :
        if value == 'Name' :
            self.settings_lyt.setCurrentWidget(self.name_wgt)
        elif value == 'Csv' :
            self.settings_lyt.setCurrentWidget(self.csv_wgt)

        #p.signals.sequences_updated.emit()
        #p.signals.shots_updated.emit()

    def add(self, add_to_kitsu=True, make_dirs=True):

        tpl_list = self.settings_lyt.currentWidget().tpl_list

        p = APP.project
        for i in tpl_list.items :
            if add_to_kitsu :
                asset_type = p.asset_types[i.asset_type]

                name = p.templates.norm_title(i.name)

                kitsu_asset = gazu.asset.new_asset(p.id, asset_type.id, name, i.description )
                #APP.kitsu.assets.APPend(asset)
                kitsu_asset['asset_type_id'] = asset_type.id

                # Create Tasks
                tasks =[]
                for t in p.asset_task_types :
                    assignees = []
                    if self.source.value == 'Name' and t.name in self.assign_tasks.value :
                        assignees = [APP.user.data]
                    task = gazu.task.new_task(kitsu_asset, t.id, assignees=assignees )

                kitsu_asset['tasks'] = tasks

                asset = p.assets.add(kitsu_asset)

            if make_dirs :
                dirs = asset.tpl_file.make_dirs(i.data)
                print('Make dir', dirs)

        #app.signals.assets_updated.emit()


class AddEntityDlg(VDialog):
    def __init__(self, parent=None, type='asset'):
        super().__init__(parent, title=f'Add {type.title()}', Background='dark',
        Padding='normal', Spacing='small')

        self.resize(*APP.resize(420, 480) )

        self.setWindowFlags(self.windowFlags() | Qt.Tool)

        if type =='asset' :
            self.entity_wgt = AssetWidget(self)
        elif type =='shot' :
            self.entity_wgt = ShotWidget(self)
        elif type =='sequence' :
            pass

        self.create_folder = Bool(True, name='Create Folders', align='Right')
        self.add_to_kitsu = Bool(True, name='Add to Kitsu', align='Right')
        self.layout.row(self.create_folder, self.add_to_kitsu)

        self.add_btn = PushButton(text= 'Create')
        self.add_btn.clicked.connect(self.add_entity)

        self.layout.row(self.add_btn, align='Right')

    def add_entity(self) :
        self.entity_wgt.add()

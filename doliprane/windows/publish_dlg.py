
from PySide2.QtCore import Qt
#from PySide2.QtGui import *
from PySide2.QtWidgets import QApplication

from doliprane.widgets.properties import *
from doliprane.widgets.basic_widgets import *
from doliprane.constants import APP

import gazu
import os


class PublishDlg(VDialog) :
    def __init__(self, parent=None, connect=None):
        super().__init__(parent, title='Publish', Spacing='small', Padding='normal')

        self.resize(*APP.resize(340, 0) )

        form_widget = FWidget(self, Spacing='small')

        self.status = ComboBoxExpand(items=['TODO','WIP', 'WFA', 'DONE'])
        self.status.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        form_widget.layout.row('Status :', self.status)

        self.comment = Text()
        form_widget.layout.row('Comment :', self.comment)

        self.playblast = Bool()
        self.playblast.setFocusPolicy( Qt.NoFocus )
        form_widget.layout.row('Make Playblast:', self.playblast)


        self.ok = PushButton(self, text= 'OK')

        if connect :
            self.ok.clicked.connect(connect)

        self.ok.clicked.connect(self.close)

from PySide2.QtCore import *
from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QAbstractItemView, QSizePolicy

from pathlib import Path

from doliprane.widgets.properties import *
from doliprane.widgets.basic_widgets import *
from doliprane.widgets.custom_widgets import Thumbnail, ActionButton
from doliprane.windows.playblast_dlg import PlayblastDlg
from doliprane.windows.publish_dlg import PublishDlg
from doliprane.constants import *
from doliprane.utils import open_file

import os
import template

'''
class OpenTaskDlg(VDialog) :
    def __init__(self, version, local_version, parent=None, connect=None):
        super().__init__(parent, title='Warning', Background='dark',
        Padding='normal', Spacing='normal')

        APP = QApplication.instance()
        self.resize(*APP.resize(320, 5) )
        self.task = version.task
        self.version = version
        self.local_version = version

        self.label = TextLabel(self, text=f'A newer version {version.name} is available on the network:')
        self.button_group = QButtonGroup()

        if local_version :
            cb = CheckBox(self, text=f'Open local version {local_version.name}')
            cb.action = 'open'
        else :
            cb = CheckBox(self, text='Build version and open it')
            cb.action = 'build'

        self.button_group.addButton(cb)

        cb = CheckBox(self, text=f'Copy network version {version.name} and open it')
        cb.action = 'synchronise'
        self.button_group.addButton(cb)

        cb.setChecked(True)

        if connect :
            self.valided.connect(connect)

        self.ok = PushButton(self, text='OK', connect=self.valid)

    def valid(self) :
        action = self.button_group.checkedButton()
        if action == 'open' :
            self.local_version.open()
        elif action == 'build' :
            version = self.task.build()
            version.open()
        elif action == 'synchronise' :
            version = self.version.synchronise()
            version.open()

        self.close()
'''


class TaskLabel(HWidget):
    def __init__(self, parent, task):
        super().__init__(parent, align='Left')

        self.frame = HWidget(self, align='Left', Height='normal', Rpadding='normal',
        Spacing='small',Background='normal')
        self.frame.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        self.task_color = Frame(self.frame , Width='micro', Height='normal')
        self.task_color.setStyleSheet(f"background-color: {task.task_type.color};")
        #print(task.task_type.color, f"background-color: {task.task_type.color};")
        self.task_lbl = TextLabel(self.frame , task.name, FontWeight='normal')

        self.setMinimumWidth(75)

        #Frame(self, Size='micro')

        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

class TaskWidget(HWidget) :
    def __init__(self, task):
        super().__init__(align = 'Left', Spacing='normal', Margin='normal')


        self.is_opened = False
        APP = QApplication.instance()
        prefs = APP.preferences


        self.setAcceptDrops(True)
        self.task = task
        self.entity = task.entity

        self.thumbnail_lbl = Thumbnail(self)
        #self.thumbnail_lbl.tooltip.connect

        self.task_label = TaskLabel(self, task)
        #self.task_color = Frame(self, Width='micro', Height='small')
        #self.task_color.setStyleSheet(f"background-color: {task.task_type.color};")
        #print(task.task_type.color, f"background-color: {task.task_type.color};")
        #self.task_lbl = TextLabel(self, task.name, FontWeight='normal')

        #if task.task_type in APP.project.shot_task_types and task.work_in_sequence :
        #    self.entity_lbl = TextLabel(self, 'Sequence'+'/'+self.entity.sequence.name)
        #else :


        self.entity_lbl = TextLabel(self, self.entity.label)

        #for k, v in data.items() :
        #    print(k,v)
        self.layout.addStretch()
        self.status_btn = PushButton(self, tag='Status', FontSize='small')
        self.set_status()
        self.task.status_updated.connect(self.set_status)
        #self.type.setFocusPolicy(Qt.NoFocus)

        #Frame(self, tag='big')



        self.soft =PushButton(self, icon=QIcon(), tag='Transparent')


        self.version_enum = Enum(parent=self, IconSize='tiny')
        self.version_enum.setFocusPolicy(Qt.NoFocus)
        self.version_enum.setMinimumWidth(65)
        #self.set_versions()
        self.version_enum.value_changed.connect(self.set_screenshot)
        self.task.versions_updated.connect(self.set_versions)
        self.set_versions()
        #self.version_enum.setVisible(not prefs.general.work_in_local)

        #local_versions = task.local_versions.keys()
        #self.local_version_enum = Enum(parent=self, items=local_versions if local_versions else ['None'] )
        #self.local_version_enum.setFocusPolicy(Qt.NoFocus)
        #self.local_version_enum.setVisible(prefs.general.work_in_local)

        self.open = PushButton(self, icon='open_32',
        IconSize= 'small', tooltip='Open Task',
        connect=self.on_open_clicked )

        #APP.signals.connection_updated.connect(self.on_connection_update)

        #self.on_connection_update()
        '''
    def on_connection_update(self, connection) :
        print('on_connections_update')
        #version_name = self.version_enum.value
        icon=QIcon()

        #print([Path(c.filepath) for c in APP.server.connections])

        #if version_name != 'None' :
            #active_version = self.task.versions[version_name]
            #print(active_version.local_path)
        if connection.filepath :
            path = Path(connection.filepath)
            #print('Paths :', paths)
            #print('Versions :', [v.local_path for v in self.task.versions])

            for version in self.task.versions :
                if version.local_path == path :
                    self.version_enum.value = version.name
                    icon = QIcon(self.task.soft.icon.as_posix() )
                    break

        self.soft.setIcon(icon)
        '''



    def set_status(self) :
        status = self.task.status
        c = status.color.darker(135)
        self.status_btn.setStyleSheet(f'background : rgb{c.red(), c.green(), c.blue()}; color: rgb(240,240,240)')
        self.status_btn.setText(status.norm_name)

    def set_screenshot(self) :
        if not APP.preferences.general.display_thumbnails :
            return

        version_name = self.version_enum.value
        #print('version_name', version_name)
        if not version_name or version_name == 'None' :
            pixmap = None
        else :
            pixmap = self.task.versions[version_name].screenshot


        print('\n\n<>>>>>>>  Set screenshot', APP.preferences.general['display_thumbnails'])
        self.thumbnail_lbl.set_pixmap(pixmap)

    def set_versions(self) :
        self.version_enum.clear()

        if self.task.versions :
            for v in self.task.versions :
                local_exist, server_exist = v.local_path.exists(), v.path.exists()
                icon = QIcon()
                if local_exist and server_exist :
                    icon = Icon('on_local_and_server_16')
                elif local_exist :
                    icon = Icon('on_local_16')
                elif server_exist :
                    icon = Icon('on_server_16')

                self.version_enum.addItem(icon, v.name )

            self.version_enum.value = self.task.versions[-1].name

        else :
            self.version_enum.addItem('None' )



        self.set_screenshot()


    def dragEnterEvent(self, e):
        list_wgt = self.item.listWidget()

        if e.mimeData().hasText():
            e.accept()

            list_wgt.setCurrentItem(self.item)

            for i in range(list_wgt.count() ) :
                item = list_wgt.item(i)
                item.setSelected(item is self.item)

        else:
            e.ignore()
            print ("ignore")

    def dropEvent(self, e):
        print ("drop", e.mimeData().text())

        files = [Path(u.toLocalFile() ) for u in e.mimeData().urls()]
        file = Path(files[0])

        if file.suffix == '.blend' :
            self.task.insert_file(file)

        elif file.suffix == '.png' :
            self.entity.set_preview(file)

        elif file.is_dir() :
            print('Dir')

    def on_open_clicked(self, modifiers) :
        prefs = APP.preferences
        version_name = self.version_enum.value

        # Open the folder
        if modifiers == Qt.ControlModifier and version_name != 'None':
            active_version = self.task.versions[version_name]
            return open_file(active_version.path.parent)

        if version_name == 'None' :
            self.task.build(background=False)
            #self.set_versions()
            return

        version = self.task.versions[version_name]

        if prefs.general.work_in_local :
            if not version.local_path.exists() :
                version.download()
            version.open()
        else :
            version.open()


class MyTask(VWidget) :
    def __init__(self, parent=None):
        super().__init__(parent, Padding='normal', Background='dark', Spacing='normal')

        self.setWindowTitle('My Task')



        APP.kitsu.my_tasks_ready.connect(self.set_ui)

        # Signals
        #APP.kitsu.my_tasks_ready.connect(self.set_my_tasks)
        APP.signals.connection_updated.connect(self.on_connection_update)

    def set_ui(self) :

        self.layout.clear()

        self.search_bar = HWidget(self)

        self.task_status = ComboBoxExpand(parent=self.search_bar,
        items = ['Task to do', 'Done', 'Timesheets'], connect=self.filter)
        self.task_status.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Fixed)

        self.search_ledit = LineEdit(self.search_bar, icon='search',
        clear_btn=True, connect=self.filter)


        Frame(self.search_bar, tag='small')
        #self.playlast_btn = PushButton(self.search_bar, icon='playblast_32',
        #IconSize= 'small', tooltip='Playblast Selected Versions',
        #connect = self.on_playblast_clicked)
        self.playblast_dlg = PlayblastDlg(self, connect=self.on_playblast_clicked)
        #playblast_action = APP.project.shots.actions['Playblast']
        self.playlast_btn = PushButton(parent=self.search_bar, icon='playblast_32',
        IconSize= 'small', tooltip='Playblast Selected Versions', connect=self.playblast_dlg.exec_)


        Frame(self.search_bar, tag='normal')
        self.play_btn = PushButton(self.search_bar, icon='play_32',
        IconSize= 'small', tooltip='Play Selected Tasks',
        connect = self.on_play_clicked )

        Frame(self.search_bar, tag='normal')
        self.build = PushButton(self.search_bar, icon='build_32',
        IconSize= 'small', tooltip='Build Selected Tasks',
        connect = self.on_build_clicked )

        Frame(self.search_bar, tag='very_small')
        self.publish_dlg = PublishDlg(self, connect=self.on_publish_clicked)
        self.publish = PushButton(self.search_bar, icon='comment_40',
        IconSize= 'normal', tooltip='Publish Selected Tasks',
        connect = self.publish_dlg.show )

        Frame(self.search_bar, tag='normal')
        self.upload = PushButton(self.search_bar, icon='upload_32',
        IconSize= 'small', tooltip='Upload Selected Versions to Server',
        connect = self.on_upload_clicked )

        self.list_wgt = VListWidget(self, tag='AssetList')
        self.list_wgt.setSelectionMode(QAbstractItemView.ExtendedSelection)

        self.list_wgt.itemSelectionChanged.connect(
        lambda: setattr(APP, 'selected_tasks', self.get_selected_tasks() ) )

        self.list_wgt.itemSelectionChanged.connect(
        lambda: setattr(APP, 'selected_versions', self.get_selected_versions() ) )


        self.set_my_tasks()


    def get_selected_tasks(self) :
        #print([ w.entity.name for w in self.list_wgt.selectedWidgets() ])

        return [ w.task for w in self.list_wgt.selectedWidgets() ]

    def get_selected_versions(self) :
        selected_versions = []
        for w in self.list_wgt.selectedWidgets() :
            version_name = w.version_enum.value
            if version_name != 'None':
                selected_versions.append( w.task.versions[version_name] )

        return selected_versions




    def set_connection(self, connection) :
        for w in self.list_wgt.widgets() :
            print(w)


    def on_connection_update(self, connection) :
        icon = QIcon()
        #path = connection.filepath
        '''
        entity = None
        data = None

        data = APP.project.templates['asset_file'].parse(path)
        if data :
            entity = APP.project.assets.get(data['name'])

        if not entity :
            data =APP.project.templates['shot_file'].parse(path)
            if data :
                entity = APP.project.shots.find(number= data['shot'],
                sequence_number=data['sequence'])
        '''

        #print('Connection Update')
        entity = connection.entity
        if entity :
            for task in entity.tasks :
                task.set_versions()

            #for w in self.list_wgt.widgets() :
            #    w.set_versions()
                #w.set_screenshot()



        '''
        #print([Path(c.filepath) for c in APP.server.connections])

        #if version_name != 'None' :
            #active_version = self.task.versions[version_name]
            #print(active_version.local_path)
        datas = [c.entity for c in APP.server.connections]
        print('Paths :', paths)
        print('Versions :', [v.local_path for v in self.task.versions])

        for version in self.task.versions :
            if version.local_path in paths :
                self.version_enum.value = version.name
                icon = QIcon(self.task.soft.icon.as_posix() )
                break

        self.soft.setIcon(icon)
        '''

    def on_play_clicked(self) :
        '''
        reel = []
        for w in self.list_wgt.selectedWidgets() :
            data = w.task.format_data
            #print(data)
            output =  w.entity.tpl_output.find(data, version=template.LAST, ext='mov')
            #print(output)
            if output :
                reel.append(output)

        APP.project.softs['mrViewer'].launch(file=reel)
        '''



        outputs = []
        for t in self.get_selected_tasks() :
            output =  t.tpl_output.find(t.format_data,
            version=template.LAST, ext='mov')

            if output :
                outputs.append(output)

        APP.project.shots.play(shots=outputs)

    def on_playblast_clicked(self) :
        #print(versions)
        APP.project.shots.playblast(
            versions = self.get_selected_versions(),
            update_libraries = self.playblast_dlg.update_libraries.value,
            burn_stamps = self.playblast_dlg.burn_stamps.value,
            add_to_kitsu = self.playblast_dlg.add_to_kitsu.value,
            play_on_finish = self.playblast_dlg.play_on_finish.value
        )

    def on_upload_clicked(self) :
        for v in self.get_selected_versions() :
            v.upload()

    def on_build_clicked(self) :
        for t in self.get_selected_tasks() :
            t.build(background=True, local=False) # Build on Network
            t.set_versions()

    def on_publish_clicked(self) :
        kitsu = APP.kitsu

        status = self.publish_dlg.status.currentText()
        comment = self.publish_dlg.comment.value

        for w in self.list_wgt.selectedWidgets() :
            version_name = w.version_enum.value
            if version_name != 'None':
                version = w.task.versions[version_name]

                version.publish(status=status, comment=comment)
            w.task.update()
            #w.set_status()


    def filter(self) :
        task_status = self.task_status.currentText()

        for w in self.list_wgt.widgets() :
            task = w.task
            visibility = False

            if task_status == 'Task to do' :
                visibility = task.status.short_name.lower() != 'done'

            elif task_status == 'Done' :
                visibility = task.status.short_name.lower() == 'done'

            if self.search_ledit.text().lower() not in task.entity.name.lower() :
                visibility = False
            #if search_ledit.text() not self.task.entity.name

            w.item.setHidden(not visibility)

    def set_my_tasks(self) :
        self.list_wgt.clear()

        tasks = APP.project.my_tasks()
        user = APP.user

        if not APP.project : return
        '''
        ## Shot tasks
        shot_tasks = [t for t in tasks if t.task_types in APP.project.shot_task_types]

        ## Check for task that work in sequences
        sequence_tasks = {}
        for t in shot_tasks :
            if t.work_in_sequence :
                seq_name = t.entity.sequence.name
                if not seq_name in sequences :
                    sequence_tasks[seq_name] = []
                else :
                    sequence_tasks[seq_name].append(t)

                shot_tasks.remove(t)


        for seq_name, shot_tasks in sequence_tasks :
            #task.set_versions()

            task_widget = TaskWidget(task)
            self.list_wgt.addWidget(task_widget,
        '''

        for task in tasks :
            task.set_versions()

            task_widget = TaskWidget(task)
            self.list_wgt.addWidget(task_widget, task.name)


        self.filter()

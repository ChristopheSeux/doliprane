from PySide2.QtCore import Qt, QThread
#from PySide2.QtGui import *
from PySide2.QtWidgets import QApplication, QAbstractItemView, QAction, QMenu, QWidgetAction

from doliprane.widgets.properties import *
from doliprane.widgets.basic_widgets import *
from doliprane.widgets.custom_widgets import *
#from doliprane.custom_widgets import *
from doliprane.constants import *

from doliprane.windows.add_entity_dlg import AddEntityDlg

import os
import shutil
import gazu
from os.path import exists
import template
from doliprane.constants import APP


class ShotWidget(HWidget) :
    def __init__(self, shot):
        super().__init__(Hpadding='normal', Vmargin='normal')
        #print(data)
        self.entity = shot
        self.label = TextLabel(self, f'{shot.sequence.name}/{shot.name}')


class ShotsList(VWidget):
    type = 'Shots'
    def __init__(self, parent=None):
        super().__init__(parent, align='Top')

        self.search_bar = HWidget()
        self.search_ledit = LineEdit(self.search_bar, icon='search', clear_btn=True,
        connect=self.filter )

        #Frame(self.search_bar, tag='small')
        #self.encode_btn = PushButton(self.search_bar, 'Encode')
        #self.encode_btn.clicked.connect(self.encode)


        Frame(self.search_bar, tag='small')
        self.add_entity_dlg = AddEntityDlg(self, type='shot')
        self.add_btn = PushButton(self.search_bar, '+ Add')
        self.add_btn.clicked.connect(lambda : self.add_entity_dlg.show())


        Frame(self.search_bar, tag='small')
        self.playlast_btn = PushButton(self.search_bar, icon='playblast_32')
        self.playlast_btn.clicked.connect(self.playblast_shots)
        #self.playlast_btn = WidgetAction(self.search_bar, icon='playblast_32', action=)

        Frame(self.search_bar, tag='small')
        self.play_btn = PushButton(self.search_bar, icon='play_32')
        self.play_btn.clicked.connect(self.play_shots)


        Frame(self.search_bar, tag='small')
        self.download_btn = PushButton(self.search_bar, icon='download_32',
        connect = self.on_download_btn_clicked,
        IconSize='small', tooltip='Download Shot to Local')

        Frame(self.search_bar, tag='big')
        self.actions_dir = ACTIONS_DIR
        self.actions_menu = Menu()
        self.actions_btn = PushButton(self.search_bar, icon='console_40')
        self.actions_btn.setMenu(self.actions_menu)

        APP.signals.project_setted.connect(self.set_actions)

        #self.filter_wgt = HWidget(self)
        self.tasks_list = ListEnum(self)
        self.tasks_list.value_changed.connect(
        lambda x : setattr(APP, 'active_task_name', x ) )
        #self.filter_wgt.layout.addStretch()

        Frame(self, tag='small')
        self.list_wgt = VListWidget(self,tag='AssetList')
        self.list_wgt.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.list_wgt.setContextMenuPolicy(Qt.ActionsContextMenu)
        #quitAction = QAction("Quit", self.list_wgt)

        '''
        table = QTableWidget()
        table.insertRow(0)
        table.insertRow(1)
        table.insertColumn(0)
        table.setHorizontalHeaderItem(0, QTableWidgetItem("Sequence"))
        table.insertColumn(1)
        self.layout.addWidget(table)

        btn1 = TextLabel(text='Toto')
        btn1.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        btn2 = TextLabel(text='Tata')
        btn2.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        label = TextLabel(text='Tata',props={'Vmargin':'big'})
        label.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)

        table.setCellWidget(0, 0, btn1)
        table.setCellWidget(0, 1, btn2)
        table.setCellWidget(1, 0, label)
        #table.setCellWidget(1, 0, PushButton(text='Test'))
        table.setShowGrid(False)

        table.resizeRowsToContents()
        table.resizeColumnsToContents()
        table.horizontalHeader().setSectionResizeMode(QHeaderView.Fixed)
        table.verticalHeader().setSectionResizeMode(QHeaderView.Fixed)

        item = QTableWidgetItem()
        item.setFlags(Qt.NoItemFlags)
        table.setItem(0, 0, item)
        '''
        #table.item(0,0).setFlags(Qt.NoItemFlags)
        #table.horizontalHeader().setStretchLastSection(True)

        # Signals
        self.list_wgt.itemSelectionChanged.connect(
        lambda: setattr(APP, 'selected_shots', self.get_selected_shots() ) )

        APP.signals.shots_updated.connect(self.set_shot_list)

        self.list_wgt.value_changed.connect(self.on_item_changed)

        APP.signals.task_types_updated.connect(self.set_task_types)


    # Make a generic encoding
    '''
    def encode(self) :
        from doliprane.base_objects.encoding import Encoding

        T = APP.project.templates

        task=self.tasks_list.value.lower()
        shots_data = []

        for shot in self.get_selected_shots() :
            output =  shot.tpl_output.find({**shot.format_data, **{'shot_task':task} },
            version=template.LAST, ext='mov')

            data = {
                **shot.to_dict(),
                'filepath' : output,
                'name': T['blur_shot_name'].format(shot.format_data)
            }

            shots_data.append( data)

        print(shots_data)

        stamps = [
        {'value': '{name}', 'position' : 'bottom_left'},
        {'value': '{frame: 04d}', 'position' : 'bottom_right'},
        {'value': '{timecode}', 'position' : 'top_right'},
        {'value': '{date}', 'position' : 'top_right'}
        ]

        encoding = Encoding(shots_data, stamps, 1280, 720)
        encoding.render("/home/graph/Bureau/draw_text/stamp.mov")
        #print(shots_data)
    '''

    def set_actions(self) :
        print('\n >>>>> Set Actions')

        for action in APP.project.shots.actions :
            if action.name in ('Playblast', 'Play') : continue # Ignore Standard Action
            #ActionButton
            action_widget = QWidgetAction(self.actions_menu)
            action_widget.setDefaultWidget(ActionButton(action, parent=self, draw_text=True))
            self.actions_menu.addAction(action_widget)

            print('Set Shots Actions', action.name)
        """
        for action_path in self.actions_dir.glob('*.py') :
            print(action_path)
            action = PythonAction(self.actions_menu, action_path)

        print('')
        self.actions_menu.addSeparator()
        """

    def get_selected_shots(self) :
        return [w.entity for w in self.list_wgt.selectedWidgets()]

    def set_task_types(self) :
        self.tasks_list.clear()
        for t in APP.project.shot_task_types :
            #print(t.name)
            self.tasks_list.addItem(t.name)


    def playblast_shots(self) :
        print(APP.selected_shots)

    def play_shots(self) :
        task=self.tasks_list.value.lower()
        outputs = []
        for shot in self.get_selected_shots() :
            output =  shot.tpl_output.find({**shot.format_data, **{'shot_task':task} },
            version=template.LAST, ext='mov')

            if output :
                outputs.append(output)

        APP.project.shots.play(shots=outputs )

        '''
        reel = []
        for w in self.list_wgt.selectedWidgets() :
            data = w.entity.format_data
            data['shot_task'] = self.tasks_list.value.lower()
            output =  w.entity.tpl_output.find(data, version=template.LAST, ext='mov')
            if output :
                reel.append(output)


        APP.project.softs['mrViewer'].launch(file=reel)

        '''




    def contextMenuEvent(self, event):
        contextMenu = QMenu(self)
        remove_shot_action = contextMenu.addAction("Remove Shot")
        action = contextMenu.exec_(self.mapToGlobal(event.pos()))

        if action == remove_shot_action :
            print('REMOVE SHOTS')
            for w in self.list_wgt.selectedWidgets() :
                w.entity.remove()
                self.list_wgt.removeWidget(w)
            #print([i.entity.name for i in self.list_wgt.selectedWidgets()])


    def on_download_btn_clicked(self) :
        for w in self.list_wgt.selectedWidgets() :
            w.entity.download()



    def filter(self) :
        search = self.search_ledit.text().lower()
        for w in self.list_wgt.widgets() :
            shot_name = w.entity.name.lower()
            sequence_name = w.entity.sequence.name.lower()
            w.item.setHidden(search not in shot_name and search not in sequence_name)
        return


    def on_item_changed(self, item_name) :
        widget = self.list_wgt.currentWidget()
        if not widget : return

        entity = widget.entity



        #print(entity)
        #print(entity.casting)
        #print([i.asset.name for i in entity.casting])
        return

    def set_shot_list(self) :
        self.list_wgt.clear()

        for shot in sorted(APP.project.shots, key=lambda s : f'{s.sequence.norm_name}_{s.norm_name}') :
            shot_widget = ShotWidget(shot)
            self.list_wgt.addWidget(shot_widget, shot.name)






class LibraryList(VWidget) :
    type = 'Library'
    def __init__(self, parent=None):
        super().__init__(parent, align='Top')

        self.search_bar = HWidget()
        self.search_ledit = LineEdit(self.search_bar, icon='search', clear_btn=True,
        connect=self.filter )


    def filter(self) :
        return


class SequenceWidget(HWidget) :
    def __init__(self, sequence):
        super().__init__(Hpadding='normal', Vmargin='normal')
        #print(data)
        self.entity = sequence
        self.label = TextLabel(self, f'{sequence.name}')


class SequencesList(VWidget) :
    type = 'Sequences'
    def __init__(self, parent=None):
        super().__init__(parent, align='Top')

        self.search_bar = HWidget()
        self.search_ledit = LineEdit(self.search_bar, icon='search', clear_btn=True,
        connect=self.filter )
        APP.signals.sequences_updated.connect(self.set_sequence_list)

        self.list_wgt = VListWidget(self, tag='AssetList')
        self.list_wgt.setSelectionMode(QAbstractItemView.ExtendedSelection)

    def set_sequence_list(self) :
        self.list_wgt.clear()

        for seq in sorted(APP.project.sequences, key=lambda s : s.name) :
            #print('Add Sequence Widget')
            seq_widget = SequenceWidget(seq)
            self.list_wgt.addWidget(seq_widget, seq.name)

    def filter(self) :
        return


class AssetWidget(HWidget) :
    def __init__(self, data):
        super().__init__(Padding='normal', Spacing='small')

        asset = data['asset']
        #image = kitsu.get_image(data)
        self.entity = asset
        self.asset_type = asset.type.norm_name
        self.name = asset.name

        self.thumbnail_lbl = Thumbnail(self, None )
        self.set_preview()

        self.thumbnail_lbl.double_clicked.connect(self.on_thumbnail_double_clicked)

        self.entity.preview_updated.connect(self.set_preview)

        self.label = TextLabel(self, asset.name, FontWeight='bold')


        self.layout.addStretch()

        #self.moodboard_btn = PushButton(self, icon='moodboard_40', tag='Transparent')
        #self.data = data

        #self.name = data['name']
        #entity_types = {v['id'] : v['name'] for k,v in kitsu.asset_types.items()}
        #print(data)
        #self.type = data['asset_type_name']
        self.setAcceptDrops(True)


    def on_thumbnail_double_clicked(self) :
        # Open moodboard or create empty if not existing
        print('Open moodboard')

        #print([s.name for s in APP.project.softs])
        '''
        md = {
        "window": {
            "size": [860, 720],
            "pos": [2846, 214]
        },
        "view": {
            "pos": [-280, -222],
            "transform": [
                0.18806446489600004,
                0.0,
                0.0,
                0.18806446489600004,
                73.99999798391201,
                61.99999520853601
            ],
            "zoom": 0.18806446489600004
        },
        "scene": {
            "items": []
                }
        }
        '''

        #print(self.entity.moodbard)
        #w = Dialog(parent=APP.main_window)
        #w.setWindowFlags(self.windowFlags() | Qt.SubWindow)
        #w.setParent(APP.main_window)

        #PushButton(parent = w,text='test')

        #w.show()


        APP.project.softs['MoodBoard'].launch(file=self.entity.moodbard)




    def set_preview(self) :
        if not APP.preferences.general.display_thumbnails :
            return
        self.thumbnail_lbl.set_pixmap(self.entity.thumbnail)

    def dragEnterEvent(self, e):
        list_wgt = self.item.listWidget()

        if e.mimeData().hasText():
            e.accept()

            list_wgt.setCurrentItem(self.item)

            for i in range(list_wgt.count() ) :
                item = list_wgt.item(i)
                item.setSelected(item is self.item)

        else:
            e.ignore()
            print ("ignore")

    def dropEvent(self, e):
        print ("drop", e.mimeData().text())

        files = [Path(u.toLocalFile() ) for u in e.mimeData().urls()]
        file = Path(files[0])

        if file.suffix == '.blend' :
            self.entity.tasks['modeling'].insert_file(file)

        elif file.suffix == '.png' :
            self.entity.set_preview(file)

        elif file.is_dir() :
            print('Dir')


class AssetLoadThread(QThread) :
    asset_loaded = Signal(dict)

    def run(self) :
        for asset in sorted(APP.project.assets, key=lambda a : a.norm_name) :
            #print(asset.name)
            data = {'asset' : asset}
            data['thumbnail'] = QImage(asset.thumbnail.as_posix() )
            self.asset_loaded.emit( data )

            #print(data)

            #QCoreApplication.processEvents()


class AssetsList(VWidget):
    type = 'Assets'
    def __init__(self, parent=None):
        super().__init__(parent, Spacing='small')

        #self.filter_wgt = HWidget(self, spacing=6, margin=(10,0,10,0))
        APP.signals.assets_updated.connect(self.set_asset_list)
        APP.signals.entity_types_updated.connect(self.set_asset_types)

        self.search_bar = HWidget()
        self.search_ledit = LineEdit(self.search_bar, icon='search', clear_btn=True,
        connect=self.filter )

        Frame(self.search_bar, tag='small')

        self.add_entity_dlg = AddEntityDlg(self, type='asset')
        self.add_btn = PushButton(self.search_bar, '+ Add')
        self.add_btn.clicked.connect(lambda : self.add_entity_dlg.show())

        Frame(self.search_bar, tag='small')
        self.actions_dir = ACTIONS_DIR
        self.actions_btn = PushButton(self.search_bar, icon='console_40')

        self.actions_menu = Menu()
        self.set_actions()

        self.actions_btn.setMenu(self.actions_menu)

        Frame(self.search_bar, tag='big')
        self.download_btn = PushButton(self.search_bar, icon='download_32',
        connect = self.on_download_btn_clicked,
        IconSize='small', tooltip='Download Asset to Local')

        self.upload_btn = PushButton(self.search_bar, icon='upload_32',
        connect = self.on_upload_btn_clicked,
        IconSize='small', tooltip='Upload Asset to Server')

        self.asset_type = ListEnum(self )
        self.asset_type.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.asset_type.itemSelectionChanged.connect(self.filter)
        self.asset_type.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        #self.layout.row(self.asset_type,'')
        #self.filter_wgt.layout.addStretch()

        self.list_wgt = VListWidget(self, tag='AssetList')
        self.list_wgt.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.list_wgt.value_changed.connect(self.on_item_changed)

        #print(self.layout.property('Spacing'))
        self.asset_load_thread = None


    def on_upload_btn_clicked(self) :
        for w in self.list_wgt.selectedWidgets() :
            entity = w.entity
            entity.tasks['modeling'].set_versions()
            w.entity.upload(versions=[entity.tasks['modeling'].versions[-1]])


    def on_download_btn_clicked(self) :
        for w in self.list_wgt.selectedWidgets() :
            entity = w.entity
            entity.tasks['modeling'].set_versions()
            w.entity.download(versions=[entity.tasks['modeling'].versions[-1]])


    def set_actions(self) :
        return
        print('SetActions')
        for action_path in self.actions_dir.glob('*.py') :
            print(action_path)
            action = PythonAction(self.actions_menu, action_path)

        print('')
        self.actions_menu.addSeparator()

    def on_item_changed(self, item_name) :

        widget = self.list_wgt.currentWidget()
        if not widget : return

        entity = widget.entity

        print(entity.root)
        print(entity.local_root)

        return
        print(item_name)
        '''
        widget = self.list_wgt.currentWidget()
        if not widget :
            return


        #print(widget)
        #print(widget.data)

        tpl = APP.project.templates

        name = tpl.norm_str(widget.name)
        type = tpl.norm_str(widget.type)

        data = dict(asset_type=type, name=name)
        data = tpl.norm_types(data)

        path = tpl['asset_dir'].format(data)
        '''

        #print('Path', path, tpl['asset_file'].find_dirs(data) )

    def filter(self) :
        search = self.search_ledit.text().lower()
        asset_types = [i.text.lower() for i in self.asset_type.selectedItems()]

        if not asset_types : return

        for w in self.list_wgt.widgets() :
            asset_type = w.entity.type.name.lower()
            asset_name = w.entity.name.lower()
            w.item.setHidden(asset_type not in asset_types or search not in asset_name)

    def set_asset_types(self) :
        self.asset_type.clear()
        for t in APP.project.asset_types :
            self.asset_type.addItem(t.name)


    def set_asset_list(self) :
        self.list_wgt.clear()
        #assets = gazu.client.fetch_all(f"projects/{p['id']}/assets")
        if self.asset_load_thread and self.asset_load_thread.isRunning() :
            self.asset_load_thread.terminate()

        self.asset_load_thread = AssetLoadThread()
        self.asset_load_thread.asset_loaded.connect(self.set_asset)
        self.asset_load_thread.finished.connect(self.filter)
        self.asset_load_thread.start()

    def set_asset(self, data) :
        asset_widget = AssetWidget(data)
        i = self.list_wgt.addWidget(asset_widget, asset_widget.name)

        asset_types = [i.text.lower() for i in self.asset_type.selectedItems()]
        i.setHidden(asset_widget.asset_type not in asset_types )


class AssetBrowser(VWidget) :
    def __init__(self, parent=None):
        super().__init__(parent, Spacing='small', Padding='normal', Background='dark')

        self.setWindowTitle('Asset Browser')

        #Top Bar
        self.search_bar = HWidget(self)
        self.search_bar.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)

        self.entity_list_wgt = StackedWidget(self)

        self.entity = {
            'Assets' : AssetsList(self.entity_list_wgt),
            'Shots' : ShotsList(self.entity_list_wgt),
            'Sequences' : SequencesList(self.entity_list_wgt),
            'Library' : LibraryList(self.entity_list_wgt),
        }


        self.entity_enum = Enum(parent=self.search_bar,
        items=list(self.entity.keys() ))
        self.entity_enum.value_changed.connect(self.on_entity_changed)

        self.search_stack = StackedLayout(parent = self.search_bar.layout)
        for k, v in self.entity.items() :
            self.search_stack.addWidget(v.search_bar)

        self.restore_settings()
        APP.signals.closed.connect(self.save_settings)

    def get_selected_entities(self) :
        entity_type = self.entity_enum.value
        list_wgt = self.entity_list_wgt.layout.currentWidget().list_wgt

        return list_wgt.selectedWidgets()

    def restore_settings(self) :
        value= APP.settings.value('entity','')
        if value :
            self.entity_enum.value = value

    def save_settings(self):
        APP.settings.setValue('entity', self.entity_enum.value )

    def on_entity_changed(self, value) :
        self.entity_list_wgt.layout.setCurrentWidget(self.entity[value])
        self.search_stack.setCurrentWidget(self.entity[value].search_bar)
        #self.add_entity_dlg.entity.value = value[:-1]

#from PySide2.QtCore import *
#from PySide2.QtGui import *
from PySide2.QtWidgets import QApplication

import os
from pathlib import Path

from doliprane.widgets.custom_widgets import *
from doliprane.widgets.basic_widgets import *
from doliprane.widgets.properties import *
from doliprane.constants import *
import json


class PrefsWindow(HDialog) :
    def __init__(self, parent=None):
        super().__init__(parent, title='Preferences', Background='very_dark')

        self.resize(*APP.resize(360, 480) )
        APP.signals.project_setted.connect(self.set_project_ui)

        APP.signals.closed.connect(self.save_settings)
        APP.signals.closed.connect(self.save_project_settings)

        self.tabs = TabWidget(self)

        self.set_ui()


    def set_ui(self) :

        # General Tab
        self.general_tab = FWidget(title='General',
        Background='dark', Spacing='normal', Padding='normal')

        self.work_in_local = APP.preferences.general['work_in_local']
        self.general_tab.layout.row('', self.work_in_local )

        self.display_thumbnails = APP.preferences.general['display_thumbnails']
        self.general_tab.layout.row('', self.display_thumbnails )

        self.local_root = APP.preferences.general['local_root']

        self.local_root.value_changed.connect(self.on_local_root_changed)

        self.general_tab.layout.row('Local Root', self.local_root )

        self.projects_ui_list = UIList(name='')
        self.general_tab.layout.row('Projects', self.projects_ui_list )

        self.envs_ui_list = UIList(name='')
        self.general_tab.layout.row('Environments', self.envs_ui_list )

        self.tabs.addTab(ScrollArea(widget=self.general_tab), self.general_tab.windowTitle())

        self.restore_settings()

    def set_project_ui(self) :

        self.tabs.clear()
        self.set_ui()
        #self.tabs = TabWidget(self)

        self.local_root.default_value = APP.project.env.data.get('LOCAL_ROOT', '')
        self.local_root.value = self.local_root.default_value

        # Env Tab
        self.env_tab = FWidget(title='Envs',
        Background='dark', Spacing='normal', Padding='normal')

        for k, v in APP.project.env.data.items() :
            if isinstance(v, str) :
                self.env_tab.layout.row(k, String(value=v) )
            elif isinstance(v, int) :
                self.env_tab.layout.row(k, Int(value=v, min=None, max=None, reset=True) )
            elif isinstance(v, float) :
                self.env_tab.layout.row(k, Float(value=v, min=None, max=None, reset=True) )

        # Display Tab
        self.display_tab = FWidget( title='Display',
        Background='dark', Spacing='normal', Padding='normal')

        APP.preferences.display['ui_scale'].value_changed.connect(lambda x : setattr(APP, 'ui_scale', x))
        self.display_tab.layout.row('UI Scale', APP.preferences.display['ui_scale'])
        self.display_tab.layout.row('brightness', APP.preferences.display['brightness'])

        for t in (  self.env_tab, self.display_tab ) :
            self.tabs.addTab(ScrollArea(widget=t), t.windowTitle())

        self.restore_project_settings()

    def on_local_root_changed(self, value) :
        value = Path(value).as_posix()
        #os.environ['LOCAL_ROOT'] = value
        APP.project.env['LOCAL_ROOT'] = value
        #print('Local ROOT changed')
        #APP.project.env.set()

    def save_settings(self):
        APP.settings.setValue('ui_scale', APP.ui_scale )
        APP.settings.setValue('projects', self.projects_ui_list.to_list() )
        APP.settings.setValue('envs', self.envs_ui_list.to_list() )

    def restore_settings(self) :
        ui_scale = APP.settings.value('ui_scale', None)
        if ui_scale is not None :
            APP.preferences.display['ui_scale'].value = float(ui_scale)

        ## Projects conf list
        projects = APP.settings.value('projects', [])
        if not projects or not any(projects):
            projects= []#CONFIG['projects']

        if not isinstance(projects, (list, tuple)) :
            projects = [projects]

        for p in projects :
            if not p : continue
            self.projects_ui_list.add( str(p) )

        ## Envs conf list
        envs = APP.settings.value('envs', [])
        if not isinstance(envs, (list, tuple)) :
            envs = [envs]

        for e in envs :
            if not e : continue
            self.envs_ui_list.add( str(e) )


    def save_project_settings(self):
        if not APP.project : return
        local_root = Path(self.local_root.value).as_posix()
        APP.project.settings.setValue('local_root', local_root )
        APP.settings.setValue('work_in_local', self.work_in_local.value )

    def restore_project_settings(self) :
        if not APP.project : return

        local_root = APP.project.settings.value('local_root', None)
        if local_root is not None:
            self.local_root.value = local_root
            #value = Path(value).as_posix()
            #APP.project.env['LOCAL_ROOT'] = value
        work_in_local = APP.settings.value('work_in_local', None)
        if work_in_local is not None :
            if isinstance(work_in_local, str) :
                work_in_local = json.loads(work_in_local)
            self.work_in_local.value = work_in_local

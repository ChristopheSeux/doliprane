
import argparse
import json

def run_python_cmds(cmds) :
    pass

if __name__ == '__main__' :
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--task-versions', required=True)

    if '--' in sys.argv :
        index = sys.argv.index('--')
        sys.argv = [sys.argv[index-1],*sys.argv[index+1:]]

    args = parser.parse_args()
    args.task_versions = json.loads(args.task_versions)


    run_python_cmds(**vars(args))

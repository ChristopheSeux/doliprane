import subprocess
import platform
from pathlib import Path
import tempfile
import os
from zipfile import ZipFile


def open_file(filepath, env=None) :

    if platform.system() == 'Darwin':       # macOS
        subprocess.Popen(['open', filepath], env=env)
    elif platform.system() == 'Windows':    # Windows
        subprocess.Popen(['start', filepath], env=env)
    else:                                   # linux variants
        subprocess.Popen(['xdg-open', filepath], env=env)


def get_func_from_path(self, path, func_name) :
    from importlib import util

    spec = util.spec_from_file_location(func_name, path)
    mod = util.module_from_spec(spec)

    spec.loader.exec_module(mod)

    return getattr(mod, func_name)

def unzip(src, dst=None) :
    if not dst :
        dst = Path(src).parent

    if platform.system() == 'Windows' :
        with ZipFile(str(src), 'r') as zipObj:
            zipObj.extractall(str(dst))
    else :
        subprocess.call(['unzip', str(src), '-d', str(dst)])


def copy_file(src, dst, only_new) :
    if (dst.exists() and only_new and dst.stat().st_mtime>=src.stat().st_mtime) :
        return

    print(f'Copy file from {src} to {dst}')
    if platform.system() == 'Windows' :
        subprocess.call(['copy', str(src), str(dst)], shell=True)
    else :
        subprocess.call(['cp', str(src), str(dst)])

    #subprocess.call(cmd)

def copy(src, dst, only_new=True, excludes=[]) :
    src = Path(src)
    dst = Path(dst)

    if dst.is_dir() :
        dst.mkdir(exist_ok=True, parents=True)
    else :
        dst.parent.mkdir(exist_ok=True, parents=True)

    #if platform.system() == 'Windows' :
    if src.is_file() :
        copy_file(src, dst, only_new)

    elif src.is_dir() :
        src_files = [f for f in src.rglob('*') if not any([e in str(f) for e in excludes]) ]
        dst_files = [dst/f.relative_to(src) for f in src_files]

        for src_file, dst_file in zip(src_files, dst_files)  :
            if src_file.is_dir():
                dst_file.mkdir(exist_ok=True, parents=True)
            else :
                copy_file(src_file, dst_file, only_new)
        '''
        options = ['/E', '/F', '/I','/R', '/Y']
        options = []
        cmd = ['xcopy']
        #if only_new :
        #    cmd += ['/D']
        if excludes :
            exclude_file = tempfile.NamedTemporaryFile(mode='w+', delete=False, suffix='.txt')
            exclude_file.write('\n'.join(excludes))
            options += [f'/EXCLUDE:{exclude_file.name}']

        cmd += [str(src), str(dst), ''.join(options)]
        '''

    #else :
    #    cmd +=['cp', str(src), str(dst)]

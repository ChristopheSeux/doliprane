import bpy
import sys
import json

args = json.loads(sys.argv[-1])

bpy.ops.wm.read_homefile(use_empty=True)

collection = bpy.data.collections.new(args['name'])
bpy.context.scene.collection.children.link(collection)

bpy.ops.wm.save_as_mainfile(filepath=args['path'])

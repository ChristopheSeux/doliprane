from PySide2.QtCore import QSettings
from PySide2.QtGui import QGuiApplication
from PySide2.QtWidgets import QApplication


from doliprane.base_objects import PreferenceProps, StyleSheet, Project, Signals, Kitsu, Collection, Person
from doliprane.base_objects.worker import Worker
from doliprane.constants import *
from doliprane.widgets.basic_widgets import *
from doliprane.widgets.custom_widgets import PythonAction
from doliprane.widgets.properties import *
from doliprane.windows import *
from doliprane.server import ServerThread

from pathlib import Path

from env import Env
import doliprane as dlp
import sys
import os


class MainWindow(VWidget) :
    def __init__(self, parent=None):
        super().__init__(title='Doliprane', icon='app_icon_32',
        Background='very_dark')

        #print('PYTHONPATH', os.environ['PYTHONPATH'])
        #print('sys.path', sys.path)
        APP.ui_scale = 1.0
        APP.main_window = self
        APP.settings = QSettings('settings', 'doliprane')
        APP.preferences = PreferenceProps()
        APP.signals = Signals()

        APP.selected_shots = []
        APP.selected_assets = []
        APP.selected_versions = []
        APP.selected_task = []
        APP.active_task_name = ''

        APP.kitsu = Kitsu()

        self.start_server()

        APP.worker = Worker()


        screen = QGuiApplication.primaryScreen()
        APP.pixel_ratio = screen.logicalDotsPerInchX()/96.0
        APP.resize = lambda x, y : [i *APP.pixel_ratio for i in (x,y)]
        ##Init Env and template

        #APP.envs = [Env(p.stem) for p in Path(ENV_DIR).glob('*.yml') if p.stem !='global']
        #APP.studio = Studio(CONFIG)
        #APP.conf = CONFIG


        self.set_env()
        self.set_projects()


        #projects = [ Project(p) for p in project_conf ]
        #projects = []


        #envs = [Env(*CONFIG['envs'], Path(p)/'env.yml' ) for p in CONFIG['projects']]


        style_props = (APP.preferences.display, APP.preferences.theme)
        APP.stylesheet = StyleSheet(STYLESHEET, STYLE, style_props)

        self.top_bar = HWidget(self.layout, Spacing='very_small', Padding='small', Background='dark')

        self.btn_menu = PushButton(self.top_bar, icon='menu', tag='Transparent',
        cursor='PointingHandCursor', IconSize='big',
        connect=lambda : self.menu_wgt.setVisible(not self.menu_wgt.isVisible()) )

        self.project = Enum(parent=self.top_bar, items=APP.projects.keys() )
        self.project.value_changed.connect(self.set_project)
        self.top_bar.layout.addStretch()


        self.login_dialog = LoginDlg(self)
        ## Btn Refresh
        self.btn_refresh = PushButton(self.top_bar, icon='refresh',
        tag='Transparent', IconSize='big', Size='big', cursor='PointingHandCursor',
        connect=APP.kitsu.fetch )

        ## Btn Script
        self.btn_script = PushButton(self.top_bar, icon='console_48',
        tag='Transparent', IconSize='big', Size='big', cursor='PointingHandCursor',
        connect=None  )


        ### Script Menu
        #self.actions_dir = ACTIONS_DIR
        self.script_menu = Menu()
        #self.set_actions()

        self.btn_script.setMenu(self.script_menu)


        ## Btn kitsu Connect
        self.btn_user = PushButton(self.top_bar, icon='user',
        IconSize='big', Size='big', tag='Transparent', cursor='PointingHandCursor',
        connect = lambda : self.login_dialog.exec() )

        ## Btn Settings
        self.prefs_window = PrefsWindow(self)
        self.btn_settings = PushButton(self.top_bar, icon='settings',
        IconSize='big', Size='big', tag='Transparent', cursor='PointingHandCursor',
        connect=lambda : self.prefs_window.show() )

        self.body = HWidget(self.layout)

        ### Menu Widget
        self.menu_wgt = VWidget(self.body, align='Top', Background='medium', Hpadding='big')
        label = TextLabel(self.menu_wgt, text='User')
        label.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        self.browse = PushButton(self.menu_wgt, text='Browse', tag='Menu')
        self.my_tasks = PushButton(self.menu_wgt, text='My Tasks', tag='Menu')
        self.farm = PushButton(self.menu_wgt, text='Farm', tag='Menu')
        self.player = PushButton(self.menu_wgt, text='Player', tag='Menu')

        label = TextLabel(self.menu_wgt, text='STUDIO')
        label.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        self.productions = PushButton(self.menu_wgt, text='Productions', tag='Menu')
        self.people = PushButton(self.menu_wgt, text='People', tag='Menu')

        label = TextLabel(self.menu_wgt, text='ADMIN')
        label.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        self.softs_btn = PushButton(self.menu_wgt, text='Softs', tag='Menu')
        self.settings = PushButton(self.menu_wgt, text='Settings', tag='Menu')

        #self.menu_wgt.addStretch()

        self.menu_wgt.hide()

        self.tabs = TabWidget(self.body)

        self.softs_tab = Softs()
        self.my_tasks_tab = MyTask()
        self.asset_browser_tab = AssetBrowser()

        for t in (self.softs_tab, self.my_tasks_tab, self.asset_browser_tab ) : #, Player(), Farm()] :
            self.tabs.addTab(t, t.windowTitle())

        self.restore_settings()

        self.resize(*APP.resize(560, 680))

    def set_projects(self) :
        project_confs = APP.settings.value('projects', [])
        if not isinstance(project_confs, (list, tuple) ) :
            project_confs = [project_confs]

        # Try to create Project :
        projects = []
        for p in project_confs :
            print('PROJECT CONF', p)
            if not p or not Path(p).exists() :
                print(f'The path of the project {p} does not exist')
                continue
            #try :
            projects.append( Project(p) )
            #except :
            #print(f'Could not create project {p}')

        APP.projects = Collection.from_attr('name', projects)
        APP.project = None


    def set_env(self) :
        env_confs = APP.settings.value('envs', [])
        if not isinstance(env_confs, (list, tuple) ) :
            env_confs = [env_confs]

        APP.envs = []
        for e in env_confs :
            if not e or not Path(e).exists() :
                print(f'The path of the env {e} does not exist')
                continue

            APP.envs.append( Env(e) )

    def start_server(self) :
        ## Launch Doliprane Server
        APP.server = ServerThread()
        APP.server.start()

    def set_actions(self) :
        self.script_menu.clear()

        for action in APP.project.actions :
            self.script_menu.addAction( PythonAction(self.script_menu, action) )

        #self.script_menu.addSeparator()


    def set_project(self, project_name) :
        if not project_name : return

        #env = next(e for e in APP.envs if e.data['PROJECT_NAME'] == project_name)
        APP.project = APP.projects[project_name]
        #APP.project.env.set()

        python_paths = os.environ.get('PYTHONPATH','').split(os.pathsep)
        os.environ['PYTHONPATH'] = os.pathsep.join(python_paths+[str(MODULE_DIR.parent)] )

        #APP.env = APP.projects[project_name].env# Env(env.name)

        self.login_dialog.connect_to_kitsu()

        print(f'Project set {APP.project.name}')

        # Set app
        self.softs_tab.set_softs()
        self.set_actions()

        APP.signals.project_setted.emit()


    def restore_settings(self) :
        index = APP.settings.value('active_tab', None)
        if index is None : index = 0
        self.tabs.setCurrentIndex(int(index))

        project = APP.settings.value('project' , None)
        if project is None and APP.projects : project = APP.projects[0].name
        self.project.value = project

    def closeEvent(self, event):
        APP.signals.closed.emit()
        print('Close Event')

        self.save_settings()
        super().closeEvent(event)

    def save_settings(self):
        APP.settings.setValue('active_tab', self.tabs.currentIndex() )
        APP.settings.setValue('project', self.project.value )

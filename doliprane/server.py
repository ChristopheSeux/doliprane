import socket
import pickle
from PySide2.QtCore import QThread
from PySide2.QtWidgets import QApplication

from doliprane.constants import APP

import socket
import threading


#Client class, new instance created for each connected client
#Each instance has the socket and address that is associated with items
#Along with an assigned ID and a name chosen by the client
class Client(QThread):
    def __init__(self, server, client, address):
        super().__init__()
        self.client = client
        self.address = address
        self.buffer_size = 4096

        self.signal = True
        self.server = server

        self.filepath = None
        self.version = None
        self.data = {}
        self.entity = None
        self.versions = []

        self.app = QApplication.instance()

        self.send(['GET', {'filepath': 'bpy.data.filepath'}])

    def __str__(self):
        return str(self.address)

    def receive(self) :
        msg = b''
        while self.signal :
            try :
                print('[SERVER] Waiting for client input')
                chunk = self.client.recv(self.buffer_size)
            except Exception as e :
                #print(e)
                self.close()
                break

            if chunk == b'' :
                self.close()
                break

            msg+= chunk
            if not chunk or len(chunk) < self.buffer_size: break

        if msg :
            return pickle.loads(msg)

    def close(self) :
        print(f'[SERVER] The server is closing the connection with client {self.address}')

        self.client.shutdown(2)
        self.client.close()
        self.filepath = None
        self.server.connections.remove(self)
        self.app.signals.connection_updated.emit(self)
        self.signal = False

    def send(self, msg) :
        d = pickle.dumps(msg)
        self.client.sendall(d)

        print('[SERVER] Message sent', msg)

    def run(self):
        p = self.app.project

        while self.signal:
            msg = self.receive()

            if not msg : break

            print('[SERVER] Data Receive from client :', msg)

            if msg == 'Exit' :
                self.close()

            elif isinstance(msg, (list, tuple)) and len(msg) == 2 :
                action_name, data = msg
                if action_name == 'COMMENT' :
                    versions = [v for v in self.versions if v.task_name == data['task']]
                    if versions :
                        version = versions[0]
                        version.publish(status=data['status'], comment = data['comment'])
                        version.task.update()
                    #print('Comment')

                elif action_name == 'PLAYBLAST' :
                    version = self.versions[0]
                    version.playblast(file=data['file'], add_to_kitsu=data['add_to_kitsu'], play_on_finish= data['play_on_finish'])
                    #action = APP.project.shots.actions[data['name']]
                    #del data['name']
                    #action.run(**data)
                    #print('Comment')

                elif action_name == 'POST' :
                    self.data.update(data)

                    if 'filepath' in data  :
                        path = data['filepath']
                        #if not path : break

                        self.entity = None
                        #versions = []
                        # Set entity if found from path
                        self.versions = []
                        #print('\nFILEPATH', path)
                        #print('\nTEMPLATE', self.app.project.templates['asset_file'].string)
                        d = p.templates['asset_file'].parse(path)
                        if d :
                            self.entity = p.assets.get(d['name'])

                            for task in self.entity.tasks :
                                task.set_versions()
                                v = task.versions.find(version=d['version'])
                                if v :
                                    self.versions.append(v)

                        # It's not an asset path maybe it's a shot :
                        if not self.entity :
                            d = p.templates['shot_file'].parse(path)
                            if d :
                                self.entity = p.shots.find(number= d['shot'],
                                sequence_number=d['sequence'])

                                task = self.entity.tasks[d['shot_task']]
                                task.set_versions()

                                print('>>>',task, d['shot_task'])
                                v = task.versions.find( version=d['version'] )
                                print(v)
                                if v :
                                    self.versions.append(v)

                        #print('VERSIONS', versions)
                        if self.versions :
                            data = {
                                'template_file' : self.entity.tpl_file.string,
                                'template_output' : self.entity.tpl_output.string,
                                'screenshot' : self.versions[0].screenshot,
                                'file_infos' : self.versions[0].file_infos,
                                'entity_type' : self.entity.type.norm_name,
                                'task_items' : [v.task.name for v in self.versions],
                                'all_status_items' : [s.norm_name for s in p.task_status],
                                'artist_status_items' :[s.norm_name for s in p.task_status.find_all(is_artist_allowed=True)]
                            }

                            self.send(['POST', data])

                    self.app.signals.connection_updated.emit(self)

                '''
                # Find related version
                self.entity = None
                if data[0] == 'filepath' :

                    template_data = None
                    try :
                        d = self.app.project.templates['asset_file'].parse(data[1])
                        self.entity = self.app.project.assets[d['name']]
                        #version =
                    except :
                        d =self.app.project.templates['shot_file'].parse(data[1])
                        '''

        print('Closing client')




class ServerThread(QThread) :
    def __init__(self) :
        super().__init__()
        #Variables for holding information about connections
        self.connections = []

        print('Start Server Thread')
        #Get host and port
        self.host = socket.gethostname()
        self.port = 8888

        #Create new server socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #try :
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))
        self.sock.listen(5)

        signals = QApplication.instance().signals

        signals.closed.connect(self.close)

    def close(self) :
        print('Closing the connections')
        for c in self.connections :
            c.close()

        #self.sock.shutdown(2)
        self.sock.close()
        self.isRunning = False

    #Wait for new connections
    def run(self):
        while self.isRunning :
            try :
                sock, address = self.sock.accept()
            except :
                break

            client = Client(self, sock, address)
            self.connections.append(client)
            client.start()
            #QApplication.instance().signal.connection_updated.emit()

            print("[SERVER] New connection " + str(client))

        print('[SERVER] Server Thread terminate')


'''
class ServerThread(QThread):
    def __init__(self):
        super().__init__()

        self.header_size = 10
        self.buffer_size = 4096
        self.is_running = True
        self.host = socket.gethostname()
        self.port = 8888
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self.sock.bind((self.host, self.port))
        self.sock.listen(5)

        self.clients = []

        print('Doliprane Server initialysed')

    def receive_data(self, socket) :
        #socket.setblocking(0)
        data = b''
        while self.is_running :
            try :
                print('Receiving from client')
                chunk = socket.recv(self.buffer_size)
                print('chunk', chunk)
            except ConnectionResetError :
                print('remove client from list')
                break
            except  ConnectionAbortedError :
                print('Connexion Aborded client from list')
                break

            data+= chunk
            if not chunk or len(chunk) < self.buffer_size: break

        #socket.close()
        if data :
            return pickle.loads(data)

    def send(self, socket, message) :
        msg = pickle.dumps(message)
        socket.sendall(msg)

        print('Message sent')

    def run(self):
        while self.is_running :
            #Accept connection request.
            client, addr = self.sock.accept()

            if client not in self.clients :
                print('add client', client)
                self.clients.append(client)
                self.send(client, 'Welcome to Doliprane Server')

            for c in self.clients :
                data = self.receive_data(client)
                #print(data)
'''
